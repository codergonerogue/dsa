package com.codehouse.problems.miscellaneous.similartriangles;

import com.codehouse.problems.miscellaneous.similartriangles.model.Triangle;
import com.codehouse.problems.miscellaneous.similartriangles.model.Vertex;

import java.util.HashMap;

public class SimilarTriangles implements ISimilarTriangles {

    @Override
    public boolean areSimilar(int[] coordinates) {

        HashMap<Integer, Vertex> vertices = new HashMap<>();


        for(int i = 0; i < coordinates.length; i = i + 2) {
            Vertex vertex = new Vertex(coordinates[i], coordinates[i+1]);
            vertices.put((i/2), vertex);
        }


        Triangle triangle1 = new Triangle();
        triangle1.setVertex1(vertices.get(0));
        triangle1.setVertex2(vertices.get(1));
        triangle1.setVertex3(vertices.get(2));

        Triangle triangle2 = new Triangle();
        triangle2.setVertex1(vertices.get(3));
        triangle2.setVertex2(vertices.get(4));
        triangle2.setVertex3(vertices.get(5));



        return false;
    }
}
