package com.codehouse.problems.miscellaneous.similartriangles.model;

import java.util.HashMap;

public class Triangle {

    public enum Vertices {
        A, B, C;
    }

    private Vertex vertex1;
    private Vertex vertex2;
    private Vertex vertex3;
    private HashMap<Vertices, Vertex> triangleVertices;

    public Triangle(HashMap<Vertices, Vertex> trangleVertices) {

    }

    public Triangle() {

    }

    public Vertex getVertex1() {
        return vertex1;
    }

    public void setVertex1(Vertex vertex1) {
        this.vertex1 = vertex1;
    }

    public Vertex getVertex2() {
        return vertex2;
    }

    public void setVertex2(Vertex vertex2) {
        this.vertex2 = vertex2;
    }

    public Vertex getVertex3() {
        return vertex3;
    }

    public void setVertex3(Vertex vertex3) {
        this.vertex3 = vertex3;
    }
}
