package com.codehouse.problems.miscellaneous.similartriangles;

public interface ISimilarTriangles {
    boolean areSimilar(int[] coordinates);
}
