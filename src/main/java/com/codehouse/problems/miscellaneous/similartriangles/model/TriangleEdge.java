package com.codehouse.problems.miscellaneous.similartriangles.model;

import java.util.HashMap;

public class TriangleEdge {

    HashMap<Edges, Double> edgeLengths = new HashMap<>();

    public enum Edges {
        AB, BC, AC;
    }

    public HashMap<Edges, Double> getEdgeLengths(Triangle triangle) {
        double lengthAB = getLength(triangle.getVertex1(), triangle.getVertex2());
        double lengthBC = getLength(triangle.getVertex1(), triangle.getVertex3());
        double lengthAC = getLength(triangle.getVertex1(), triangle.getVertex3());

        edgeLengths.put(Edges.AB, lengthAB);
        edgeLengths.put(Edges.BC, lengthBC);
        edgeLengths.put(Edges.AC, lengthAC);

        return edgeLengths;
    }

    private double getLength(Vertex v1, Vertex v2) {
        double xSquare = Math.pow((v2.getX() - v1.getX()), 2);
        double ySquare = Math.pow((v2.getY() - v1.getY()), 2);
        double lengthSquared = xSquare + ySquare;
        return Math.sqrt(lengthSquared);
    }

}
