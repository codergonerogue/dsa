package com.codehouse.problems.miscellaneous.restapiproblems;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;


public class GetMovies {
    /*
     * Complete the function below.
     */

    private static String BASE_URL = "https://jsonmock.hackerrank.com/api/movies/search/?Title=%s&page=%d";
    private static final String HTTP_METHOD = "GET";

    static String[] getMovieTitles(String substr) {
        List<String> titleList = new ArrayList<>();
        int pageNumber = 1;
        URL url = null;
        if (substr == null || substr.trim().isEmpty()) {
            return new String[0];
        }

        //Get connection
        HttpURLConnection connection = GetMovies.getConnection(substr, pageNumber);

        //get response
        ResponseData response = GetMovies.getResponseData(connection);
        int totalPages = response.getTotal_pages();
        int totalTitles = response.getTotal();
        String[] titles = new String[totalTitles];

        if(totalPages > pageNumber) {
            while(pageNumber <= totalPages) {
                response = GetMovies.getResponseData(GetMovies.getConnection(substr, pageNumber));
                for(MovieData movieData : response.getData()) {
                    titleList.add(movieData.getTitle());
                }
                pageNumber++;
            }
        }
        for(int i = 0; i < titleList.size(); i++) {
            titles[i] = titleList.get(i);
        }

        Arrays.sort(titles);
        return titles;
    }

    //Construct the request url and get the connection object
    private static HttpURLConnection getConnection(String substr, int pageNumber) {

        HttpURLConnection connection = null;
        URL url = null;
        try {
            url = new URL(String.format(BASE_URL, substr, pageNumber));
        } catch (MalformedURLException e) {
            //Do something with if exception is raised.
        }

        try {
            connection = (HttpURLConnection) url.openConnection();
            connection.setRequestMethod(HTTP_METHOD);
            connection.setRequestProperty("content-type", "application/json");
        } catch (IOException e) {
            //Do something with if exception is raised.
        }

        return connection;
    }

    private static ResponseData getResponseData(HttpURLConnection connection) {
        StringBuffer response = new StringBuffer();
        try {
            BufferedReader inputReader = new BufferedReader((new InputStreamReader(connection.getInputStream())));
            String responseLine;
            while ((responseLine = inputReader.readLine()) != null) {
                response.append(responseLine);
            }
        } catch (IOException e) {
            //Do something with if exception is raised.
        }

        Gson gson = new GsonBuilder().serializeNulls().create();
        return gson.fromJson(response.toString(), ResponseData.class);
    }

    public class ResponseData {
        private int page;
        private int per_page;
        private int total;
        private int total_pages;
        private List<MovieData> data;

        public int getPage() {
            return page;
        }

        public int getPer_page() {
            return per_page;
        }

        public int getTotal() {
            return total;
        }

        public int getTotal_pages() {
            return total_pages;
        }

        public List<MovieData> getData() {
            return data;
        }
    }

    public class MovieData {
        private String Poster;
        private String Title;
        private String Type;
        private String Year;
        private String imdbID;

        public String getPoster() {
            return Poster;
        }

        public String getTitle() {
            return Title;
        }

        public String getType() {
            return Type;
        }

        public String getYear() {
            return Year;
        }

        public String getImdbID() {
            return imdbID;
        }
    }

    public static void main(String[] args) {
        GetMovies.getMovieTitles("Spiderman");
    }
}
