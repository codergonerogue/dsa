package com.codehouse.problems.miscellaneous.hashmap;

import java.util.HashMap;

/*
 * To execute Java, please define "static void main" on a class
 * named CreateRansom.
 *
 * If you need more classes, simply define them inline.
 */

class CreateRansom {

    private static HashMap<Character, Integer> characterMap = new HashMap<Character, Integer>();

    public static void testOutputWithExpected(boolean testValue, boolean expectedValue) {
        System.out.println("Test " + testValue + " . Expected: " + expectedValue);
    }

    public static void main(String[] args) {
        String book = "aabc?";
        buildCharacterMapUtil(book);
        testOutputWithExpected(canWriteRansom("aaabc", book), false);
//        buildCharacterMapUtil(book);
//        testOutputWithExpected(canWriteRansom("aabc", book), true);
    }

    public static boolean canWriteRansom(String message, String book) {
        for(char c : message.toCharArray()) {
            Integer charcaterCount = characterMap.get(c);
            if( charcaterCount != null && charcaterCount > 0) {
                characterMap.put(c, characterMap.get(c) - 1);
                continue;
            } else {
                return false;
            }
        }
        return true;
    }

    public static void buildCharacterMapUtil(String book) {
        for(char c : book.toCharArray()) {
            if(characterMap.get(c) == null) {
                characterMap.put(c, 1);
            } else {
                characterMap.put(c, characterMap.get(c) + 1);
            }
        }
    }

}


