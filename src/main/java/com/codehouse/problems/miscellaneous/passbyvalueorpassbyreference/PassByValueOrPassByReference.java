package com.codehouse.problems.miscellaneous.passbyvalueorpassbyreference;

public class PassByValueOrPassByReference {

    public static void main(String[] args) {

        ClassA obj = new ClassA();

        System.out.println("Object created in main before passing: " + obj);

        PassByValueOrPassByReference passByValueOrPassByReference = new PassByValueOrPassByReference();
        passByValueOrPassByReference.doSomethingElse(obj);

        System.out.println("Object created in main after returning: " + obj);
    }

    public void doSomethingElse(ClassA classA) {
        System.out.println("Passed object inside method: " + classA);

        ClassA obj = new ClassA();
        System.out.println("Object created in doSomethingElse method before returning: " + obj);
        classA = obj;
    }
}
