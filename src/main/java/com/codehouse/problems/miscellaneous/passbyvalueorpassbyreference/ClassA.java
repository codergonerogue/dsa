package com.codehouse.problems.miscellaneous.passbyvalueorpassbyreference;

public class ClassA {
    public void doSomething() {
        System.out.println("Do Something");
    }
}