package com.codehouse.problems.miscellaneous;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

public class FileOperations {

    static String fileName = "sample.txt";
    static String directory = System.getProperty("user.home");
    static String absolutePath = directory + File.separator + fileName;

    public static void main(String[] args) {
        try(FileWriter fileWriter = new FileWriter(absolutePath)) {
            String fileContent = "This is a sample text.";
            fileWriter.write(fileContent);
        } catch (IOException e) {
            // exception handling
        }


        try(FileReader fileReader = new FileReader(absolutePath)) {
            StringBuilder stringBuilder = new StringBuilder();

            int ch = fileReader.read();

            while(ch != -1) {
                stringBuilder.append((char)ch);
                ch = fileReader.read();
            }

            System.out.println(stringBuilder.toString());

        } catch (IOException e) {

        }
    }

    public void readInputFromResources() {
        String inputFilePath = File.separator+"inputs"+File.separator+this.getClass().getSimpleName()+".txt";

        try {
            InputStream is = ClassLoader.class.getResourceAsStream(inputFilePath);
            InputStreamReader isr = new InputStreamReader(is);
            BufferedReader br = new BufferedReader(isr);
            String line = br.readLine();

            while (line != null) {
                line = br.readLine();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
