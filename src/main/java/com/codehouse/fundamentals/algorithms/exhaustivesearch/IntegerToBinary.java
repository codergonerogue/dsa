package com.codehouse.fundamentals.algorithms.exhaustivesearch;

public class IntegerToBinary {
    static void printAllBinary(int digit, String args) {
        if(digit == 0) {
            //base case
            // The easiest case is when there are no digits to print
            System.out.println(args);
        } else {
            //recursive case
            printAllBinary(digit - 1, args+"0");
            printAllBinary(digit - 1, args+"1");
        }
    }
    static void printAllBinary(int digit) {
        printAllBinary(digit, "");
    }

    public static void main(String[] args) {
        printAllBinary(2);
    }
}
