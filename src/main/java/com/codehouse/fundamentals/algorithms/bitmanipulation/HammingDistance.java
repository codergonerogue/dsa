package com.codehouse.fundamentals.algorithms.bitmanipulation;

public class HammingDistance {
    /**
     * Hamming Distance between two numbers is the difference between the number of set bits between the two numbers.
     * Eg: int a = 5, int b = 2
     * In binary: a = 0101
     *            b = 0010
     * HammingDistance(a,b) = 3
     *
     * Eg: int a = 6, int b = 2
     *      * In binary: a = 0110
     *      *            b = 0010
     *      * HammingDistance(a,b) = 1
     */

    /**
     * 1. XOR the two numbers
     * 2. Count the number of 1's in the result by Brian Kreghans algorithm.
     */

    public int hammingDistance(int a, int b){
        int count = 0;

        int number = a ^ b;
        while(number != 0) {
            number = number & (number - 1);
            count++;
        }

        return count;
    }
}
