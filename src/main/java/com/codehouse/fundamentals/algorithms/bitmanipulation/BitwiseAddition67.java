package com.codehouse.fundamentals.algorithms.bitmanipulation;

public class BitwiseAddition67 {

        public String addBinary(String a, String b) {
            final int aLength = a.length();
            final int bLength = b.length();
            int carryBit = 0;

            if ( aLength > bLength) {
                b = padZeros(b, aLength - bLength);
            } else if (bLength > aLength) {
                a = padZeros(a, bLength - aLength);
            }


            StringBuilder resultBuilder = new StringBuilder(a.length());
            for (int i = a.length()-1; i >= 0; i--) {
                int a1 = Integer.parseInt(a.substring(i, i+1), 2);
                int b1 = Integer.parseInt(b.substring(i, i+1), 2);

                if (a1 == 0 && b1 ==0 && carryBit != 1) {
                    resultBuilder.append("0");
                    continue;
                }

                int temp =  carryBit ^ a1;

                if (temp == 1) {
                    carryBit = 0;
                } else {
                    carryBit = 1;
                }

                temp = temp ^ b1;

                if (temp == 1) {
                    carryBit = 0;
                } else {
                    carryBit = 1;
                }

                resultBuilder.append(temp);
            }

            if (carryBit == 1) {
                resultBuilder.append(carryBit);
            }

            return resultBuilder.reverse().toString();
        }

        private String padZeros(final String num, final int numberOfZeros) {
            StringBuilder builder = new StringBuilder(num);

            for (int i = 0; i < numberOfZeros; i++) {
                builder.insert(0, "0");
            }

            return builder.toString();
        }

        public static void main(String[] args) {
            BitwiseAddition67 obj = new BitwiseAddition67();
            String result = obj.addBinary("11", "1");
            System.out.println(result);
        }
}
