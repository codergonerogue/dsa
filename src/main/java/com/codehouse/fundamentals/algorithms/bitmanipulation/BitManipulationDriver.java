package com.codehouse.fundamentals.algorithms.bitmanipulation;

public class BitManipulationDriver {
    public static void main( String[] args ) {

        SingleNumber singleNumber = new SingleNumber();
        int[] numbers = {1, 2, 1, 4, 3, 2, 4, 3, 5};
        System.out.println("Single Number: "+singleNumber.SingleNumber(numbers));

        HammingDistance hammingDistance = new HammingDistance();
        int a = 5, b = 2;
        System.out.println(String.format("Hamming Distance between %d and %d is %d", a, b,hammingDistance.hammingDistance(a, b))); //3

    }
}
