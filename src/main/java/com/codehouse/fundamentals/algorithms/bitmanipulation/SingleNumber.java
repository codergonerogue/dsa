package com.codehouse.fundamentals.algorithms.bitmanipulation;

public class SingleNumber {
    /**
     * Given an array of integers, all numbers appear in a pair of two except one.
     * Find that unique number.
     */

    /**
     * Use bitwise XOR operation.
     * Rules:
     * 1. Any number XOR'ed with itself will yield 0 as result. a ^ a = 0 ;
     * 2. Any number XORed with 0 gives the number back. a ^ 0 = a
     * 3. XOR is commutative.
     *
     * if a, b, c are 3 numbers: a and b are repeated. then we have [a, b, a, c, b].
     * now ( a XOR b XOR a XOR c XOR b)
     *  => ((a XOR a) XOR (b XOR b) XOR (c))
     *  => (0 XOR 0 XOR c)
     *  = c
     */

    public int SingleNumber(int[] numbers) {
        int singleNumber = 0;
        for(int number : numbers) {
            singleNumber = singleNumber ^ number;
        }
        return singleNumber;
    }

}
