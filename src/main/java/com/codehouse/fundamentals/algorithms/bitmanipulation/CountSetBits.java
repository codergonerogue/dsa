package com.codehouse.fundamentals.algorithms.bitmanipulation;

public class CountSetBits {

//    https://stackoverflow.com/a/12381102/4054389
    public static int count_set_bits(int n){
        int count = 0;
        while(n != 0) { //If n is not 0 then there is at least one bit that needs to be cleared
            n = n & (n-1); // Clear the RIGHT MOST SET BIT
            System.out.println(n);
            count++; // Everytime loop executes we are clearing 1 bit. Hence increment count.
        }
        return count;
    }

    public static void main(String[] args) {
        System.out.println(count_set_bits(3));
    }
}
