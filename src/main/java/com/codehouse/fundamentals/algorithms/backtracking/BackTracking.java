package com.codehouse.fundamentals.algorithms.backtracking;

import java.util.ArrayList;
import java.util.List;

/**
 * Use case of recursion
 */
public class BackTracking {

    // Problem 1: Given the number of dice and a target value. Find all the combinations of the dices
    // that will get us to the target number.
    public static void diceCombinations(int numberOfDice, int target) {
        List<Integer> combintaion = new ArrayList<>();
        diceCombinations(numberOfDice, target, combintaion);
    }

    public static void diceCombinations(int numberOfDice, int target, List<Integer> combination) {
        if(numberOfDice == 0) {
            if(target == 0) {
                for(int k : combination) {
                    System.out.print(k);
                }
                System.out.println();
            }
        } else {
            //recursive case
            for(int i = 1; i <= 6; i++) {
                combination.add(i);
                diceCombinations(numberOfDice - 1, target - i, combination);
                combination.remove(combination.size() -1);
            }
        }
    }

    public static void main(String[] args) {
        diceCombinations(3, 6);
    }

}
