package com.codehouse.fundamentals.algorithms.miscellaneous.autoboxunbox;

public class AutoBoxUnBoxing {
        public static void main(String[] args) throws InterruptedException {
        int sum = 0;

        for (int i = 1000; i < 500000; i++) {
            sum += i;
            Thread.sleep(100);
        }
    }
}
