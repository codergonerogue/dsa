package com.codehouse.fundamentals.algorithms.miscellaneous;

public class MajorityVoting {
    /**
     * This algorithm is used to identify the majority element in an array of elements.
     * This works under certain conditions only:
     * - The majority element is the element that appears more than ⌊ n/2 ⌋ times
     */

    public static int findMajority(int[] numbers) {
        int count = 0;
        int majority =0;

        for(int i : numbers) {
            if(count == 0) {
                majority = i;
                count++;
            } else {
                if(i == majority) {
                    count++;
                } else {
                    count--;
                }
            }
        }
        return majority;
    }

    public static void main(String[] args) {
        int[] input = {1, 2, 2, 2, 3, 3, 3, 3, 3, 1, 1, 1, 1, 2, 2, 2, 2, 2};
        System.out.println(findMajority(input));
    }
}
