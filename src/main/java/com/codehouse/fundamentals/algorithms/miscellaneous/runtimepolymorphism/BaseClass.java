package com.codehouse.fundamentals.algorithms.miscellaneous.runtimepolymorphism;

public class BaseClass {

    public int compute(final int num) {
        System.out.println("Base Compute");
        return num * 2;
    }

}
