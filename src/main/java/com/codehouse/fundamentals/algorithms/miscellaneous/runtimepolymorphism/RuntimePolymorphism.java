package com.codehouse.fundamentals.algorithms.miscellaneous.runtimepolymorphism;

public class RuntimePolymorphism {

    public static void computeCaller(BaseClass ref, int num) {
        ref.compute(num);
    }

    public static void main(String[] args) {
        computeCaller(new DerivedClass(), 10);

        computeCaller(new BaseClass(), 10);
    }
}