package com.codehouse.fundamentals.algorithms.miscellaneous.runtimepolymorphism;

public class DerivedClass extends BaseClass {

    @Override
    public int compute(final int num) {
        System.out.println("Derived Compute");
        return 3 * num;
    }
}
