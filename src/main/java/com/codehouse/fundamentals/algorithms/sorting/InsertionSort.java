package com.codehouse.fundamentals.algorithms.sorting;

import com.codehouse.fundamentals.algorithms.sorting.api.AbstractSort;
import com.codehouse.fundamentals.algorithms.sorting.api.Sort;

import java.util.Date;
import java.util.Timer;

public class InsertionSort<T extends Comparable> extends AbstractSort<T> {

    public T[] sort(T[] input) {

        for (int i = 0; i < input.length ; i++) {
            for (int j = i; j > 0; j--) {
                if (less(input[j], input[j-1])) {
                    exchange(input, j-1, j);
                }
            }
        }

        return input;
    }
    public static void main(String[] args) {
        Sort<Integer> insertionSort = new InsertionSort<>();
        int len = 9;
        Integer[] input = new Integer[len];
        input[0] = 4;
        input[1] = 2;
        input[2] = 7;
        input[3] = 3;
        input[4] = 9;
        input[5] = 10;
        input[6] = 8;
        input[7] = 6;
        input[8] = 5;

        insertionSort.print(input);

        Date date = new Date();
        long start = date.getTime();
        insertionSort.sort(input);
        long end = date.getTime();
        long time = start - end;
        System.out.println("Running Time:" + time + "ms");

        insertionSort.print(input);
    }
}
