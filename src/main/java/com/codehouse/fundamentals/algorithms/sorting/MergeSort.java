package com.codehouse.fundamentals.algorithms.sorting;

public class MergeSort {

    public int[] bottomUpMergeSort(int[] input) {
        int N = input.length;
        int[] aux = new int[N];
        for (int size = 1; size < N; size = size+size) {
            for (int l = 0; l < N - size; l += size + size) {
                int low = l;
                int high = Math.min(low+size+size-1, N-1);
                int mid = low + size - 1;

                merge(input, aux, low, mid, high);
            }
        }
        return input;
    }

    public int[] recursiveMergeSort(int[] input) {
        final int high = input.length - 1;
        final int[] result = new int[input.length];
        recursiveMergeSort(input, result, 0, high);
        return input;
    }

    private void recursiveMergeSort(int[] input, int[] result, int low, int high) {
        if (high <= low) {
            return;
        }
        int mid = low + ((high - low) / 2);
        recursiveMergeSort(input, result, low, mid);
        recursiveMergeSort(input, result, mid+1, high);
        merge(input, result, low, mid, high);
    }

    private void merge(int[] input, int[] result, int low, int mid, int high) {
        int i = low;
        int j = mid+1;
        int k = low;

        while(k <= high) {
            result[k] = input[k];
            k++;
        }

        k = low;
        while(k <= high) {
            if (i > mid) {
                input[k++] = result[j++];
            } else if (j > high) {
                input[k++] = result[i++];
            } else if (result[j] <= result[i]) {
                input[k++] = result[j++];
            } else {
                input[k++] = result[i++];
            }
        }
    }

    private static void print(int[] array) {
        for (int i = 0; i < array.length; i++) {
            System.out.print(array[i]+"\t");
        }
    }

    public static void main(String[] args) {
        int[] input = new int[9];
        input[0] = 4;
        input[1] = 2;
        input[2] = 7;
        input[3] = 3;
        input[4] = 9;
        input[5] = 10;
        input[6] = 8;
        input[7] = 6;
        input[8] = 5;

        MergeSort mergeSort = new MergeSort();
        System.out.println("RECURSIVE");
        mergeSort.print(input);
        System.out.println();
        int[] result = mergeSort.recursiveMergeSort(input);
        mergeSort.print(result);



        System.out.println("\nBOTTOM-UP");
        int[] input2 = new int[9];
        input2[0] = 4;
        input2[1] = 2;
        input2[2] = 7;
        input2[3] = 3;
        input2[4] = 9;
        input2[5] = 10;
        input2[6] = 8;
        input2[7] = 6;
        input2[8] = 5;
        mergeSort.print(input2);
        System.out.println();
        int[] result2 = mergeSort.bottomUpMergeSort(input);
        mergeSort.print(result2);
    }
}
