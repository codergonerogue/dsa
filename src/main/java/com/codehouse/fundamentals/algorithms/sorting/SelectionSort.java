package com.codehouse.fundamentals.algorithms.sorting;

import com.codehouse.fundamentals.algorithms.sorting.api.AbstractSort;

public class SelectionSort<T extends Comparable> extends AbstractSort<T> {

    public T[] sort(T[] input) {
        for (int i = 0; i < input.length; i++) {
            for (int j = i+1; j < input.length; j++) {
                if (less(input[j], input[i])) {
                    exchange(input, i, j);
                }
            }
        }
        return input;
    }

    public static void main(String[] args) {
        SelectionSort<Integer> selectionSort = new SelectionSort<>();
        int len = 9;
        Integer[] input = new Integer[len];
        input[0] = 4;
        input[1] = 2;
        input[2] = 7;
        input[3] = 3;
        input[4] = 9;
        input[5] = 10;
        input[6] = 8;
        input[7] = 6;
        input[8] = 5;
        selectionSort.print(input);
        System.out.println();
        selectionSort.sort(input);
        selectionSort.print(input);
    }
}
