package com.codehouse.fundamentals.algorithms.sorting.api;

public interface Sort<T extends Comparable > {
    T[] sort(T[] input);
    void print(T[] input);
    boolean less(T v, T w);
    void exchange(T[] input, int index1, int index2);
}
