package com.codehouse.fundamentals.algorithms.sorting;

import com.codehouse.fundamentals.algorithms.sorting.api.AbstractSort;
import com.codehouse.fundamentals.algorithms.sorting.api.Sort;
import com.codehouse.utils.StdRandom;

public class QuickSort<T extends Comparable> extends AbstractSort<T> {
    @Override
    public T[] sort(T[] input) {
        // Shuffle
        StdRandom.shuffle(input);
        print(input);
        quickSort(input, 0, input.length - 1);
        return input;
    }

    private void quickSort(T[] input, int low, int high) {
        if (high <= low) {
            return;
        }
        // Pick a pivot point and partition around that point.
        // Since the array is shuffled, we can just pick the first element in the shuffled
        // array and partition the array around this element.
        int partitionIndex = partition(input, low, high);
        //recursively call quickSort
        quickSort(input, low, partitionIndex - 1);
        quickSort(input, partitionIndex+1, high);
    }

    private int partition(T[] input, int low, int high) {
        int i = low;
        int j = high+1;

        while (true) {
            while (less(input[++i], input[low])) {
                if (i == high) {
                    break;
                }
            }

            while (less(input[low], input[--j])) {
                if (j == low) {
                    break;
                }
            }

            if (i >= j) {
                break;
            }

            exchange(input, i, j);
        }
        exchange(input, low, j);
        return j;
    }
    public static void main(String[] args) {
        Integer[] input = new Integer[9];
        input[0] = 4;
        input[1] = 2;
        input[2] = 7;
        input[3] = 3;
        input[4] = 9;
        input[5] = 10;
        input[6] = 8;
        input[7] = 6;
        input[8] = 5;

        Sort<Integer> quickSort = new QuickSort<>();
        quickSort.print(input);
        quickSort.sort(input);
        quickSort.print(input);
    }
}
