package com.codehouse.fundamentals.algorithms.sorting.api;

public abstract class AbstractSort<T extends Comparable> implements Sort<T> {
    @Override
    public boolean less(T v, T w) {
        return v.compareTo(w) < 0;
    }

    @Override
    public void exchange(T[] input, int index1, int index2) {
        T temp = input[index1];
        input[index1] = input[index2];
        input[index2] = temp;
    }

    public void print(T[] input) {
        for (int i = 0; i < input.length; i++) {
            System.out.print(input[i]+"\t");
        }
        System.out.println();
    }
}
