package com.codehouse.fundamentals.algorithms.sorting;

import com.codehouse.fundamentals.algorithms.sorting.api.AbstractSort;
import com.codehouse.fundamentals.algorithms.sorting.api.Sort;

import java.util.Date;

public class ShellSort<T extends Comparable> extends AbstractSort<T> {
    @Override
    public T[] sort(T[] input) {
        int inputLength = input.length;
        // Determine the shell size
        int k = 1;
        while (k < inputLength / 3) {
            k = 3*k + 1;
        }

        while (k >= 1) {
            for (int i = k; i < inputLength; i++) {
                for (int j = i; j >= k; j = j - k) {
                    if (less(input[j], input[j-k])) {
                        exchange(input, j, j - k);
                    }
                }
            }
            k = k / 3;
        }
        return input;
    }

    public static void main(String[] args) {
        Sort<Integer> shellSort = new ShellSort<>();
        int len = 9;
        Integer[] input = new Integer[len];
        input[0] = 4;
        input[1] = 2;
        input[2] = 7;
        input[3] = 3;
        input[4] = 9;
        input[5] = 10;
        input[6] = 8;
        input[7] = 6;
        input[8] = 5;

        shellSort.print(input);

        Date date = new Date();
        long start = date.getTime();
        shellSort.sort(input);
        long end = date.getTime();
        long time = start - end;
        System.out.println("Running Time:" + time + "ms");

        shellSort.print(input);
    }
}
