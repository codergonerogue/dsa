package com.codehouse.fundamentals.algorithms.recursion;

import java.io.*;


public class Recursion {
    //How to approach a recursion problem:
    // https://youtu.be/9vIyTn7ayac

    static void printStars(int n) {
        //The if is always the base case
        if (n == 1) {
            //base case that can solve one unit of problem.
            // The easiest that we can do - In this case print 1 star
            System.out.println("*");
        }
        // The else is always the recursive case.
        // Solve it with an if else first.
        // Then you can clean up the code like repetitive lines of code like the ones on
        // lines 12 and 21 which can be pulled out to one statement.
        else {
            //recursive case that needs help.
            // May be I can do some work and pass the rest to future - me?
            System.out.print("*");
            printStars(n - 1);
        }
    }

    /**
     * Compute a number raised to an exponent
     *
     * @param base     Integer
     * @param exponent Positive Integer
     * @return base ^ exponent
     * @throws IllegalArgumentException
     */
    static int power(int base, int exponent) {
        if (exponent < 0) {
            throw new IllegalArgumentException("Only positive exponents");
        }

        if (0 == exponent) {
            //base case
            // An easy case is - exponent = 0
            return 1;
        } else {
            //recursive case
            // For example 3 ^ 7
            // Is there a way I can break a small portion of this computation and let recursion
            // do that for me?
            // 3 ^ 7 = (3 ^ 6) * (3 ^ 1)
            return base * power(base, exponent - 1);
        }
    }

    /**
     * Check if a given word is a palindrome is not.
     *
     * @param word - The word that you need check
     * @return true / false
     */
    static boolean isPalidrome(String word) {
        word = word.toLowerCase();
        if (word.length() == 0 || word.length() == 1) {
            // base case
            // if the word has only one alphabet - then its a palindrome.
            // if the word is empty string - then thats a palindrome.
            return true;
        } else {
            //recursive case
            char first = word.charAt(0);
            char last = word.charAt(word.length() - 1);
            if (first == last) {
                return isPalidrome(word.substring(0, word.length() - 2));
            } else {
                return false;
            }
        }
    }

    static void integerToBinary(int number) {
        if (number < 0) {
            System.out.print("-");
            integerToBinary(-number);
        }

        if (number < 2) {
            //base case
            System.out.print(number);
        } else {
            //recursive case
            // I will print one digit and the
            // next call will print the rest of it.
            int lastBinaryDigit = number % 2;
            int restOfBinaryDigit = number / 2;
            integerToBinary(restOfBinaryDigit);
            integerToBinary(lastBinaryDigit);
        }
    }

    static void reverseLines(String fileName) {
        try(FileReader fr = new FileReader(fileName); BufferedReader bufferedReader = new BufferedReader(fr)) {
            reverseLinesHelper(bufferedReader);
        } catch(IOException e) {
            e.printStackTrace();
        }
    }
    private static void reverseLinesHelper(BufferedReader bufferedReader) throws IOException {
        String line = readFile(bufferedReader);
        //base case
        if (line != null) {
            //recursive call
            reverseLinesHelper(bufferedReader);
            // Simplest case is to print one line.
            System.out.println(line);
        }
    }
    private static String readFile(BufferedReader bufferedReader) throws IOException {
        String currentLine = bufferedReader.readLine();
        while ( currentLine != null) {
            return currentLine;
        }
        return null;
    }

    public static void main(String[] args) throws IOException {
//        printStars(5);
//        System.out.println(power(0, 10));
//        String result = isPalidrome("malayalam") ? "Palindrome" : "Not Palindrome";
//        System.out.println(isPalidrome("") ? "Is Palindrome" : "Is Not Palindrome");
//        integerToBinary(43);
        reverseLines("/home/primebot/code/dsa/src/main/java/com/codehouse/fundamentals/algorithms/recursion/input.txt");
    }


}
