package com.codehouse.fundamentals.datastructures.graphs.algorithms.sorting;

import com.codehouse.fundamentals.datastructures.graphs.model.Graph;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;

public class TopologicalSort implements ITopologicalSort {

    private Graph graph;
    private int vertices;
    private int indegrees[];
    private List<Integer> topologicalSort = new ArrayList<>();

    public TopologicalSort(Graph graph) {
        this.graph = graph;
        this.vertices = graph.getV();
        indegrees = new int[vertices];
    }

    public void topologicalSort() {
        int countForCycle = 0;

        //Step 1: Build the array having indegrees of the vertices.
        populateVerticesIndegree(vertices);

        //Step 2: Iteration over the graph
        Queue<Integer> queue = new LinkedList<Integer>();

        // 2.1. For each of the vertices put the vertex onto the queue if the indegree is 0
        for (int i = 0; i < vertices; i++) {
            if (indegrees[i] == 0) {
                //Adding at the end of the list to simulate the addition into queue
                queue.add(i);
            }
        }

        while (!queue.isEmpty()) {
            //Removing and retrieving from front of list to simulate queue enqueueing operation.
            int vertex = queue.poll();

            topologicalSort.add(vertex);
            Iterable<Integer> adjacencyListIterable = graph.getadjacencyIterator(vertex);
            //2.2 Traverse the adjacency list for this vertex.
            for(int i : adjacencyListIterable) {
                indegrees[i] = indegrees[i] - 1;
                //2.3. During this traversal if the indegrees value of any vertex becomes zero, put it in the queue.
                if(indegrees[i] == 0) {
                    queue.add(adjacencyListIterable.iterator().next());
                }
            }

            countForCycle++;
        }

        if(countForCycle != vertices) {
            System.out.println("Graph has cycle");
        }

        for(int i : topologicalSort) {
            System.out.println(i);
        }
    }


    private void populateVerticesIndegree(int vertices) {
        for (int i = 0; i < indegrees.length; i++) {
            indegrees[i] = 0;
        }

        for (int i = 0; i < indegrees.length; i++) {
            for (int vertex : graph.getadjacencyIterator(i)) {
                indegrees[vertex] = indegrees[vertex] + 1;
            }
        }
    }

    public static void main(String[] args) {
        Graph graph = new Graph(7, false);
        graph.addEdge(0, 1);
        graph.addEdge(1, 3);
        graph.addEdge(0, 2);
        graph.addEdge(2, 4);
        graph.addEdge(3, 4);
        graph.addEdge(3, 5);
        graph.addEdge(4, 5);
        graph.addEdge(6, 2);

        TopologicalSort topSort = new TopologicalSort(graph);
        topSort.topologicalSort();
    }
}
