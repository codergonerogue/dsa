package com.codehouse.fundamentals.datastructures.graphs.problems;

import java.util.Stack;

public class NumberOfIslands200 {
    int rowNbr[] = new int[] { -1, 0, 0 , 1};
    int colNbr[] = new int[] { 0, -1, 1, 0 };

    public int numIslands(char[][] grid) {

        if (grid == null || grid.length == 0) {
            return 0;
        }

        int rows = grid.length;
        int cols = grid[0].length;

        int count = 0;

        for (int i = 0; i < rows; i++) {
            for (int j = 0; j < cols; j++) {

                if (grid[i][j] == '1') {
                    depthFirstSearch(grid, i, j);
                    count++;
                }
            }
        }
        return count;
    }

    private void depthFirstSearch(char[][] grid, int row, int col) {
        grid[row][col] = '0';

        for (int i = 0; i < 4; i++) {
            int nextRow = row + rowNbr[i];
            int nextCol = col + colNbr[i];
            if (isSafe(grid, nextRow, nextCol)) {
                depthFirstSearch(grid, nextRow, nextCol);
            }
        }
    }

    private boolean isSafe(char[][] grid, int row, int col) {
        int rows = grid.length;
        int cols = grid[0].length;
        boolean isSafe = (row >=0 && row < rows) && (col >= 0 && col < cols) && (grid[row][col] == '1');
        return isSafe;
    }

    public static void main(String[] args) {

        char[][] grid = new char[4][5];
        grid[0] = new char[] {'1', '1', '0', '1', '0'};
        grid[1] = new char[] {'0', '2', '0', '1', '0'};
        grid[2] = new char[] {'1', '1', '0', '0', '0'};
        grid[3] = new char[] {'0', '0', '0', '0', '0'};

        NumberOfIslands200 numIslands = new NumberOfIslands200();
        int islands = numIslands.numIslands(grid);
        System.out.println("NUMBER OF ISLANDS = " + islands);
    }
}
