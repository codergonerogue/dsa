package com.codehouse.fundamentals.datastructures.graphs.algorithms.shortestpath;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;

public class ShortestPathToTarget {

    /**
     * A Cell represents the position [ROW, COL] of an element in the matrix.
     */
    private class Cell {
        int i;
        int j;

        Cell(int i, int j) {
            this.i = i;
            this.j = j;
        }
    }

    /**
     * Holds the data, position and distance from the source.
     */
    private class QueueNode {
        Cell cell;
        int distance;
        int data;

        QueueNode(Cell cell, int data, int distance) {
            this.cell = cell;
            this.data = data;
            this.distance = distance;
        }
    }

    /**
     * Calculate shortest distance from source to the point where the element is found.
     */
    public int shortestPath(int numRows, int numColumns, List<List<Integer>> lot) {
        if (lot == null || lot.get(0) == null || lot.size() == 0 || lot.get(0).size() == 0) {
            return -1;
        }

        if (numRows == 0 || numColumns == 0) {
            return -1;
        }

        // A two dimensional array to keep track of cells that are already visited.
        boolean[][] visited = new boolean[lot.size()][lot.get(0).size()];
        Queue<QueueNode> queue = new LinkedList<>();

        Cell source = new Cell(0, 0);
        QueueNode queueNode = new QueueNode(source, lot.get(0).get(0), 0);
        queue.add(queueNode);
        visited[source.i][source.j] = true;


        while (!queue.isEmpty()) {

            // These arrays are used to get row and column
            // numbers of 4 neighbours of a given cell
            int[] rowNum = {-1, 0, 0, 1};
            int[] colNum = {0, -1, 1, 0};

            QueueNode currNode = queue.poll();
            Cell currentCell = currNode.cell;

            if (currNode.data == 9) {
                return currNode.distance;
            }

            for (int k = 0; k < 4; k++) {
                int row = currentCell.i + rowNum[k];
                int col = currentCell.j + colNum[k];

                // if adjacent cell is valid, has path and
                // not visited yet, enqueue it.
                if (isValid(row, col, numRows, numColumns) && lot.get(row).get(col) != 0 && !visited[row][col]) {
                    // mark cell as visited and enqueue it
                    visited[row][col] = true;

                    Cell adjCell = new Cell(row, col);
                    QueueNode adjNode = new QueueNode(adjCell, lot.get(row).get(col), currNode.distance + 1);

                    queue.add(adjNode);
                }
            }
        }
        return -1;
    }

    private boolean isValid(int currentRow, int currentCol, int row, int col) {
        return (currentRow >= 0) && (currentRow < row) &&
                (currentCol >= 0) && (currentCol < col);
    }

    public static void main(String[] args) {

        List<List<Integer>> lot = new ArrayList<>();
        List<Integer> row1 = new ArrayList<>();
        row1.add(1);
        row1.add(1);
        row1.add(1);
        List<Integer> row2 = new ArrayList<>();
        row2.add(1);
        row2.add(0);
        row2.add(1);
        List<Integer> row3 = new ArrayList<>();
        row3.add(1);
        row3.add(9);
        row3.add(1);

        lot.add(row1);
        lot.add(row2);
        lot.add(row3);

        System.out.println("Original Matrix");
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                System.out.print(lot.get(i).get(j) + " ");
            }

            System.out.println();
        }

        ShortestPathToTarget obj = new ShortestPathToTarget();
        int answer = obj.shortestPath(lot.get(0).size(), lot.get(0).size(), lot);
        System.out.println("Shortest distance = " + answer);
    }
}
