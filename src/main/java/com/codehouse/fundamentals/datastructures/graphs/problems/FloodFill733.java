package com.codehouse.fundamentals.datastructures.graphs.problems;

public class FloodFill733 {
    int MAX_ROW = 0;
    int MAX_COL = 0;
    int[] rowNum = {-1, 0, 0, 1};
    int[] colNum = {0, -1, 1, 0};
    int sourceColor = 0;
    int newColor = 0;

    public int[][] floodFill(int[][] image, int sr, int sc, int newColor) {
        boolean invalidInput = image == null ||
                image.length == 0 ||
                sr < 0 || sr >= image.length ||
                sc < 0 || sc >= image[0].length ||
                image[sr][sc] == newColor;

        if (invalidInput) {
            return image;
        }

        MAX_ROW = image.length;
        MAX_COL = image[0].length;

        sourceColor = image[sr][sc];
        this.newColor = newColor;
        depthFirstSearch(image, sr, sc);
        return image;
    }

    private void depthFirstSearch(int[][] image, final int row, final int col) {
        image[row][col] = newColor;

        for (int k = 0; k < 4; k++) {
            int newRow = row + rowNum[k];
            int newCol = col + colNum[k];
            if (isSafe(image, newRow, newCol)) {
                depthFirstSearch(image, newRow, newCol);
            }
        }
    }

    private boolean isSafe(int[][] image, int row, int col) {
        boolean isSafe = (row >=0 && row < MAX_ROW) &&
                (col >= 0 && col < MAX_COL) &&
                image[row][col] == sourceColor;

        return isSafe;
    }

    public static void main(String[] args) {

        int[][] image = new int[4][5];
        image[0] = new int[] {1, 1, 0, 1, 0};
        image[1] = new int[] {0, 3, 0, 1, 0};
        image[2] = new int[] {1, 1, 0, 0, 0};
        image[3] = new int[] {0, 0, 1, 0, 0};

        int[][] image2 = new int[2][3];
        image2[0] = new int[] {0, 0, 0};
        image2[1] = new int[] {0, 1, 1};

        FloodFill733 floodFill = new FloodFill733();
        int[][] newImage = floodFill.floodFill(image2, 1, 1, 1);

        for (int i = 0; i < image2.length; i++) {
            for (int j = 0; j < image2[0].length; j++) {
                System.out.print(image2[i][j]+"\t");
            }
            System.out.println();
        }
    }
}
