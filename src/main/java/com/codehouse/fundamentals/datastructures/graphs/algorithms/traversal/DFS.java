package com.codehouse.fundamentals.datastructures.graphs.algorithms.traversal;

import com.codehouse.fundamentals.datastructures.graphs.model.Graph;

import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;
import java.util.Stack;

public class DFS {

    private Stack<Integer> vertices;
    private Set<Integer> visited;
    private Graph graph;

    public DFS(Graph g) {
        this.graph = g;
        this.vertices = new Stack<>();
        this.visited = new HashSet<>();
    }

    public void depthFirstTraversal(int vertex) {
        visited.add(vertex);
        vertices.push(vertex);

        Iterator<Integer>[] crawlers = (Iterator<Integer>[]) new Iterator[graph.getV()];
        for (int v = 0; v < graph.getV(); v++) {
            crawlers[v] = graph.getadjacencyIterator(v).iterator();
        }

        while (!vertices.empty()) {
            int currentNode = vertices.peek();
            if (crawlers[currentNode].hasNext()) {
                int nextNode = crawlers[currentNode].next();
                if (!visited.contains(nextNode)) {
                    visited.add(nextNode);
                    vertices.push(nextNode);
                }
            } else {
                // StdOut.printf("%d done\n", v);
                System.out.print(vertices.pop());
            }
        }


    }

    public static void main(String[] args) {
        Graph g = new Graph(8, false);
        g.addEdge(0, 1);
        g.addEdge(0, 6);
        g.addEdge(0, 7);
        g.addEdge(1, 2);
        g.addEdge(1, 4);
        g.addEdge(2, 3);
        g.addEdge(4, 3);
        g.addEdge(5, 4);
        g.addEdge(6, 5);
        g.addEdge(6, 7);
        g.addEdge(7, 4);


        DFS dfs = new DFS(g);
        dfs.depthFirstTraversal(0);
    }
}
