package com.codehouse.fundamentals.datastructures.graphs.algorithms.sorting;

public interface ITopologicalSort {
    void topologicalSort();
}
