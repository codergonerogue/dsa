package com.codehouse.fundamentals.datastructures.graphs.algorithms.traversal;

import com.codehouse.fundamentals.datastructures.graphs.model.Graph;

import java.util.*;

public class BFS {

    private Graph graph;
    private Queue<Integer> vertices;
    private Map<Integer, Boolean> visited;


    public BFS(Graph graph) {
        this.graph = graph;
        this.vertices = new LinkedList<>();
        this.visited = new HashMap<>();
    }

    public void breadthFirstTraversal(int node) {
        // Mark the current node as visited.
        visited.put(node, true);
        // Add the current node to the queue
        vertices.add(node);
        // As long as the queue is not empty
        while(!vertices.isEmpty()) {
            // get the element at start of queue.
            int currentNode = vertices.poll();
            // print the node data.
            System.out.print(currentNode + " --> ");

            // add its connected components to the queue
            Iterator<Integer> adjacentNodeIterator = graph.getadjacencyIterator(currentNode).iterator();
            while(adjacentNodeIterator.hasNext()) {
                int nextNode = adjacentNodeIterator.next();
                // Check if the node is visited
                if(visited.get(nextNode) == null) {
                    // mark it as visited.
                    visited.put(nextNode, true);
                    // add it to the queue.
                    vertices.add(nextNode);
                }
            }
        }
    }

    public static void main(String[] args) {
        Graph g = new Graph(4, true);
        g.addEdge(0, 1);
        g.addEdge(0, 2);
        g.addEdge(1, 2);
        g.addEdge(2, 0);
        g.addEdge(2, 3);
        g.addEdge(3, 3);

        BFS bfs = new BFS(g);
        bfs.breadthFirstTraversal(2);
    }

}
