package com.codehouse.fundamentals.datastructures.graphs.model;

import java.util.LinkedList;

public class  Graph {

    private final int V;
    private LinkedList<Integer> adjacencyList[];
    private boolean isUndirected;

    public int getV() {
        return V;
    }

    public LinkedList<Integer>[] getAdjacencyList() {
        return adjacencyList;
    }

    public Iterable<Integer> getadjacencyIterator(int v) {
        return adjacencyList[v];
    }

    public boolean isUndirected() {
        return isUndirected;
    }

    public Graph(int v, boolean isUndirected) {
        this.V = v;
        adjacencyList = new LinkedList[V];
        this.isUndirected = isUndirected;

        for (int i = 0; i < adjacencyList.length; i++) {
            adjacencyList[i] = new LinkedList<>();
        }
    }

    public void addEdge(int v, int w) {
        adjacencyList[v].add(w);

        // undirected graph. So add edge in reverse direction.
        if(this.isUndirected) {
            adjacencyList[w].add(v);
        }
    }
}
