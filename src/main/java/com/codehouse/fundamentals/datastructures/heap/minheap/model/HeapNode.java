package com.codehouse.fundamentals.datastructures.heap.minheap.model;

public class HeapNode<T> {

    /**
     * Data part of the heap node.
     */
    private final T key;
    /**
     * Weight of the heap node.
     */
    private final int weight;

    public HeapNode(T key, int weight) {
        this.key = key;
        this.weight = weight;
    }

    public T getKey() {
        return key;
    }

    public int getWeight() {
        return weight;
    }
}
