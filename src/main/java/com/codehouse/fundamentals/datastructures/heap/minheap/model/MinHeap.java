package com.codehouse.fundamentals.datastructures.heap.minheap.model;

import com.codehouse.fundamentals.datastructures.heap.Heap;

import java.util.*;

public class MinHeap<T> implements Heap<T> {

    /**
     * This is the list of all nodes in the heap. The position of the HeapNode in the list indicates the position of
     * occurrence in the MinHeap. The order or position is determined by the weight of the HeapNode.
     */
    private List<HeapNode<T>> heapNodes;
    /**
     * A mapping between the HeapNode data and the positions of occurrence in the heap.
     * Querying the map for a data T returns the relative position of that data in the MinHeap.
     */
    private Map<T, Integer> nodePositions;

    /**
     * Construct a heap from an array of keys using the index of the element as the weight of the HeapNode
     *
     * @param keys
     */
    public MinHeap(T[] keys) {
        this();
        for (int i = 0; i < keys.length; i++) {
            addNode(keys[i], i);
        }
    }


    public MinHeap() {
        this.heapNodes = new ArrayList<>();
        this.nodePositions = new HashMap<>();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean addNode(T key, int weight) {
        HeapNode<T> newNode = new HeapNode<>(key, weight);
        heapNodes.add(newNode);
        heapifyUp(newNode);
        return heapNodes.contains(newNode);
    }

    /**
     * {@inheritDoc}
     */
    public void heapifyUp(HeapNode<T> currentNode) {
        HeapNode<T> parentNode = parent(currentNode);
        nodePositions.put(currentNode.getKey(), heapNodes.indexOf(currentNode));

        while (heapNodes.indexOf(parentNode) >= 0) {

            if (parentNode.getWeight() > currentNode.getWeight()) {
                //swap the nodes in list
                swap(parentNode, currentNode);
                // update node positions map
                nodePositions.put(parentNode.getKey(), heapNodes.indexOf(parentNode));
                nodePositions.put(currentNode.getKey(), heapNodes.indexOf(currentNode));
                // update parentNode
                parentNode = parent(currentNode);
            } else {
                break;
            }
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public HeapNode<T> poll() {
        HeapNode<T> root = heapNodes.get(0); // Get the root node of the heap.
        nodePositions.remove(root.getKey()); // delete the root nodes index mapping stored in the nodePositions map.
        heapNodes.remove(0); //delete its reference from list of nodes.

        int size = heapNodes.size();
        HeapNode<T> lastNode = heapNodes.get(size - 1);
        heapNodes.add(0, lastNode); // Copy the last node in the heap to the root node position.

        nodePositions.put(lastNode.getKey(), 0); // Update the postion mapping of last node as the root node position.
        heapNodes.remove(size - 1); // Remove the last Node

        heapifyDown(heapNodes.get(0)); // Balance the order and shape of MinHeap
        return root;
    }

    /**
     * {@inheritDoc}
     */
    public void heapifyDown(HeapNode<T> currentHeapNode) {
        HeapNode<T> leftChild = leftChild(currentHeapNode);
        HeapNode<T> rightChild = rightChild(currentHeapNode);

        while (leftChild(currentHeapNode) != null && rightChild(currentHeapNode) != null) {
            if (currentHeapNode.getWeight() > leftChild.getWeight()) {
                swap(currentHeapNode, leftChild);
                nodePositions.put(currentHeapNode.getKey(), heapNodes.indexOf(currentHeapNode));
                nodePositions.put(leftChild.getKey(), heapNodes.indexOf(leftChild));
            } else if (currentHeapNode.getWeight() > rightChild.getWeight()) {
                swap(currentHeapNode, rightChild);
                nodePositions.put(currentHeapNode.getKey(), heapNodes.indexOf(currentHeapNode));
                nodePositions.put(rightChild.getKey(), heapNodes.indexOf(rightChild));
            } else {
                //do nothing;
            }
            leftChild = leftChild(currentHeapNode);
            rightChild = rightChild(currentHeapNode);
        }
    }


    /**
     * {@inheritDoc}
     */
    @Override
    public boolean contains(T key) {
        return heapNodes.get(nodePositions.get(key)).getKey() == key;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean contains(HeapNode<T> heapNode) {
        return contains(heapNode);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public HeapNode<T> leftChild(HeapNode<T> heapNode) {
        T key = heapNode.getKey();
        int currentIndex = nodePositions.get(key);
        int indexOfLeftChild = (currentIndex * 2) + 1;
        if (indexOfLeftChild > heapNodes.size() - 1) {
            return null;
        }
        return heapNodes.get(indexOfLeftChild);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public HeapNode<T> parent(HeapNode<T> heapNode) {
        int currentIndex = heapNodes.indexOf(heapNode);
        int parentIndex = (currentIndex - 1) / 2;
        return heapNodes.get(parentIndex);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public HeapNode<T> rightChild(HeapNode<T> heapNode) {
        T key = heapNode.getKey();
        int currentIndex = nodePositions.get(key);
        int rightChildIndex = (currentIndex * 2) + 2;
        if (rightChildIndex > heapNodes.size() - 1) {
            return null;
        }
        return heapNodes.get(rightChildIndex);
    }

    private void swap(HeapNode<T> parentNode, HeapNode<T> currentheapNode) {
        Collections.swap(heapNodes, nodePositions.get(parentNode.getKey()), nodePositions.get(currentheapNode.getKey()));
    }

    public List<HeapNode<T>> getHeapNodes() {
        return heapNodes;
    }

    public Map<T, Integer> getNodePositions() {
        return nodePositions;
    }
}
