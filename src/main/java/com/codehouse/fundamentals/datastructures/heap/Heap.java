package com.codehouse.fundamentals.datastructures.heap;

import com.codehouse.fundamentals.datastructures.heap.minheap.model.HeapNode;

public interface Heap<E> {

    /**
     * Create a node with the given Key and Weight and add it to the heap.
     * Re-balances the heap by calling {@link Heap#heapifyUp(HeapNode)}
     *
     * @param key    - The value of the key stored in the {@link HeapNode}
     * @param weight - The weight/priority of the {@link HeapNode}
     * @return validates that the node was added to the heap by calling {@link java.util.List#contains(Object)}
     */
    boolean addNode(E key, int weight);

    /**
     * Re-balance the heap.
     * Accepts the reference to a {@link HeapNode} and starts rebalancing the heap from that node moving upwards.
     *
     * @param heapNode The {@link HeapNode} to begin the heapify operation.
     */
    void heapifyUp(HeapNode<E> heapNode);

    /**
     * Remove the root element from the heap.
     *
     * @return The removed root {@link HeapNode}
     */
    HeapNode<E> poll();

    /**
     * Check if the heap has a node with give {@link HeapNode#key}
     *
     * @param key
     * @return true if the Heap has the key. False otherwise.
     */
    boolean contains(E key);

    /**
     * Check if the heap has a node with give {@link HeapNode}
     *
     * @param heapNode
     * @return true if the Heap has the heapNode. False otherwise.
     */
    boolean contains(HeapNode<E> heapNode);

    /**
     * Return the Left Child of a given node in MinHeap
     *
     * @param heapNode The {@link HeapNode} whose left child you want to get.
     * @return The left child node of the {@param heapNode}
     */
    HeapNode<E> leftChild(HeapNode<E> heapNode);

    /**
     * Return the Right Child of a given node in MinHeap
     *
     * @param heapNode The {@link HeapNode} whose right child you want to get.
     * @return The right child node of the {@param heapNode}
     */
    HeapNode<E> rightChild(HeapNode<E> heapNode);

    /**
     * Return the Parent node of a given node in MinHeap
     *
     * @param heapNode The {@link HeapNode} whose parent node you want to get.
     * @return The parent node of the {@param heapNode}
     */
    HeapNode<E> parent(HeapNode<E> heapNode);

}
