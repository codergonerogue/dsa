package com.codehouse.fundamentals.datastructures.stacks.problems;

import java.util.Stack;

public class BalancedParentheses {

    public boolean isValid(String s) {
        Stack<Character> parentheses = new Stack();

        for(char c : s.toCharArray()) {
            switch(c) {
                case '(':
                    parentheses.push(')');
                    break;
                case '[':
                    parentheses.push(']');
                    break;
                case '{':
                    parentheses.push('}');
                    break;
                default :
                    if(parentheses.empty() || parentheses.pop() != c) {
                        return false;
                    }
            }
        }
        return parentheses.empty();
    }

    public static void main(String[] args) {
        String input = "((";
        BalancedParentheses bp = new BalancedParentheses();
        if(bp.isValid(input)) {
            System.out.print("YES");
        } else {
            System.out.print("NO");
        }

    }
}
