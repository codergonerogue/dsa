package com.codehouse.fundamentals.datastructures.strings.problems;

import java.util.HashMap;

public class LongestNonRepeatingSubString {
    public static int lengthOfLongestSubstring(String s) {

        // https://discuss.leetcode.com/topic/8232/11-line-simple-java-solution-o-n-with-explanation

        if(s.length()==0) return 0;
        int max=0;
        HashMap<Character,Integer> hm = new HashMap<Character,Integer>();

        for(int i=0,j=0;i<s.length();i++){
            if(hm.containsKey(s.charAt(i))){
                // j represents the start of the current string with unique characters, so If you have
                // a string like abba, when you encounter last a, you
                // want to mark the start of the string from index 2(second occurrence of b) which is
                // the last known index which holds uniqueness assumption, not from map.get(a)+1 which
                // is 1.

                // You want to move the 2nd pointer (i.e j)to the right of the repeat character thus
                // excluding it (you don't want to include it anymore since its a repeat) from your new
                // longest substring.
                j=Math.max(j,hm.get(s.charAt(i))+1);
            }
            hm.put(s.charAt(i),i);
            max=Math.max(max,i-j+1);
        }
        return max;
    }

    public static void main(String[] args) {
        String input = "abcdbcbb";
        int maxSubstringLength = lengthOfLongestSubstring(input);
        System.out.print(maxSubstringLength);
    }
}
