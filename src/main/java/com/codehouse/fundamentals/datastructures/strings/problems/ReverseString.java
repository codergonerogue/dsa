package com.codehouse.fundamentals.datastructures.strings.problems;

public class ReverseString {
    public static void reverseString(char[] s) {
        if (s == null || s.length == 0) {
            return;
        }
        int end = s.length -1;
        for (int i = 0, j = end; i <= j; i++, j-- ) {
            char temp = s[i];
            s[i] = s[j];
            s[j] = temp;
        }
    }

    public static void main(String[] args) {
        char[] input = {'H', 'E', 'L', 'L'};
        reverseString(input);
        for (int i = 0; i < input.length; i++) {
            System.out.print(input[i]+"\t");
        }
    }
}
