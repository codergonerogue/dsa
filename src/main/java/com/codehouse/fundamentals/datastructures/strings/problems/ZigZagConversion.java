package com.codehouse.fundamentals.datastructures.strings.problems;

public class ZigZagConversion {

    public static String convertToZigZag(String word, int rows) {
        //Edge Cases
        if (word == null || word == "" || rows <= 1 || rows >= word.length()) {
            return word;
        }

        //Initialize an array of String Builder with
        StringBuilder[] words = new StringBuilder[rows];
        for (int i = 0; i < words.length; i++) {
            words[i] = new StringBuilder();
        }

        int wordRow = 0;
        boolean down = true;

        for (char c : word.toCharArray()) {
            words[wordRow].append(c);
            if (wordRow == rows - 1) {
                down = false;
            } else if (wordRow == 0) {
                down = true;
            }

            if (down) {
                wordRow++;
            } else {
                wordRow--;
            }
        }

        for (int i = 1; i < rows; i++) {
            words[0].append(words[i].toString());
        }

        return words[0].toString();
    }

    public static void main(String[] args) {
        System.out.println(convertToZigZag("PAYPALISHIRING", 4));
    }

}
