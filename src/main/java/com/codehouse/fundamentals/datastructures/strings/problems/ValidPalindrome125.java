package com.codehouse.fundamentals.datastructures.strings.problems;

public class ValidPalindrome125 {
    public static int intsubtract(char a, char b) {
        return a - b;
    }
    public boolean isPalindrome(String s) {
        if (s == null || s.length() == 0) {
            return true;
        }
        int L = s.length();

        int start = 0;
        int end = s.length() - 1;
        while(start < end) {
            while( start < L && !isAlphaNumeric(s.charAt(start))) {
                start++;
            }
            while(end >= 0 && !isAlphaNumeric(s.charAt(end))) {
                end--;
            }
            if((start < end) && !compare(s.charAt(start), s.charAt(end))){
                return false;
            }
            start++;
            end--;
        }
        return true;
    }

    private boolean compare(char a, char b) {
        return Character.toLowerCase(a) == Character.toLowerCase(b);
    }

    private boolean isAlphaNumeric(char c) {
        if ((c >= 48 && c <= 57) || (c >= 65 && c <= 90) || (c >= 97 && c <= 122)) {
            return true;
        }
        return false;
    }

    public static void main(String[] args) {

        System.out.println(intsubtract('A', 'a'));

//        String s = "A man, a plan, a canal: Panama";
        String s = "0P";
        ValidPalindrome125 obj = new ValidPalindrome125();
        if (obj.isPalindrome(s)) {
            System.out.println("true");
        } else {
            System.out.println("false");
        }
    }
}
