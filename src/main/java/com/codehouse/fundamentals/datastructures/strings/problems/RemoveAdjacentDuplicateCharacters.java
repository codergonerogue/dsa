package com.codehouse.fundamentals.datastructures.strings.problems;

public class RemoveAdjacentDuplicateCharacters {

    public static String removeAdjacentDuplicates(final String input) {
        if(input == null || input.trim().isEmpty()) {
            return input;
        }

        StringBuilder resultBuilder = new StringBuilder();
        char previous = input.charAt(0);
        resultBuilder.append(previous);
        for (int i = 1; i < input.length(); i++) {
            char current = input.charAt(i);
            if(current != previous) {
                resultBuilder.append(current);
                previous = current;
            }
        }

        return resultBuilder.toString();
    }

    public static void main(String[] args) {
        String testInput = "aaaabaab";
        System.out.println(removeAdjacentDuplicates(testInput));
    }
}
