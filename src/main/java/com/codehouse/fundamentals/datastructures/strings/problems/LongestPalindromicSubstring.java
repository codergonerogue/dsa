package com.codehouse.fundamentals.datastructures.strings.problems;

public class LongestPalindromicSubstring {
    private int lo, max;

    public String longestPalindrome(String s) {

        int slen = s.length();

        if(slen < 2)
            return s;

        for (int i = 0; i < slen-1; i++) {
            extend(s,i,i);
            extend(s,i,i+1);
        }

        return s.substring(lo, lo+max);
    }

    private void extend(String s, int j, int k) {

        while ( j >= 0 && k < s.length() && s.charAt(j) == s.charAt(k)) {
            j--;
            k++;
        }

        if (max < k-j-1) {
            lo = j+1;
            max = k-j-1;
        }
    }
}
