package com.codehouse.fundamentals.datastructures.strings.problems;

public class IsPalindrome9 {
    public boolean isPalindromeSimple(int x) {
        if(x < 0) {
            return false;
        } else if (x >=0 && x < 10) {
            return true;
        }

        String str = String.valueOf(x);
        int start = 0;
        int end = str.length() - 1;
        while(start < end){
            if(str.charAt(start++) != str.charAt(end--)) return false;
        }
        return true;
    }

    public boolean isPalindrome(int x) {
        if(x < 0) {
            return false;
        } else if (x >=0 && x < 10) {
            return true;
        }

        String input = Integer.toString(x);
        StringBuilder sb = new StringBuilder();
        int L = input.length();
        for (int i = 0; i < L; i++) {
            sb.append("#");
            sb.append(input.charAt(i));
        }
        sb.append("#");

        String expandedString = sb.toString();
        int i = 0;
        int j = expandedString.length() / 2;

        while (i <= j) {
            if (expandedString.charAt(i) == expandedString.charAt(j)) {
                i++;
                j--;
            } else {
                return false;
            }
        }
        return true;
    }

    public static void main(String[] args) {
        IsPalindrome9 obj = new IsPalindrome9();
        if (obj.isPalindrome(121)) {
            System.out.println("true");
        } else {
            System.out.println("false");
        }
    }
}
