package com.codehouse.fundamentals.datastructures.strings.problems;

public class ReverseWordsInString {

    public static String reverseWordsInString(final String sentence) {

        final String[] words = sentence.trim().split(" ");
        StringBuilder resultBuilder = new StringBuilder();
        for (int i = words.length - 1; i >=0 ; i--) {
            if (!words[i].trim().isEmpty()) {
                resultBuilder.append(words[i]).append(" ");
            }
        }

        return resultBuilder.toString().trim();
    }

    public static void main(String[] args) {
//        final String input = "Roses are red violets are blue";
        String input = "   a   b ";
        System.out.println(reverseWordsInString(input));
    }
}
