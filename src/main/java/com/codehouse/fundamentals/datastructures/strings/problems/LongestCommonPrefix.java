package com.codehouse.fundamentals.datastructures.strings.problems;

public class LongestCommonPrefix {


    public static void main(String[] args) {
        String[] strs = {"geeks", "geek", "ger", "geeksfor", "geeksforgeeks"};
        System.out.println(lcpByWordMatching(strs));
    }

    public static String lcpByWordMatching(String[] strs) {
        if(strs == null || strs.length == 0) {
            return "";
        }

        if(strs.length == 1) {
            return strs[0];
        }

        StringBuilder prefixbuilder = new StringBuilder(strs[0]);
        String lcp = "";

        for(int i = 1; i < strs.length; i++) {
            lcp = lcpByWordMatchingUtil(prefixbuilder, strs[i]);
        }
        return lcp;
    }

    private static String lcpByWordMatchingUtil(StringBuilder prefixBuilder, String word) {
        int length = prefixBuilder.length();

        if(word.length() < prefixBuilder.length()) {
            length = word.length();
        }

        int i;
        for(i = 0; i < length; i++) {
            if(prefixBuilder.charAt(i) != word.charAt(i)) {
                return prefixBuilder.delete(i, prefixBuilder.length()).toString();
            }
        }

        if( i == length) {
            prefixBuilder.delete(length, prefixBuilder.length());
        }
        return prefixBuilder.toString();
    }
}
