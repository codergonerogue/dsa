package com.codehouse.fundamentals.datastructures.strings.problems;

import java.util.*;

public class GroupAnagrams {

    public static void main(String[] args) {
        GroupAnagrams groupAnagrams = new GroupAnagrams();
        String[] words = {"ate", "eat", "bat", "tea", "tab", "pot"};

        List<List<String>> result = groupAnagrams.groupAnagrams(words);
        System.out.print(result.toString());
    }

    public List<List<String>> groupAnagrams(final String[] words) {

        Map<String, List<String>> groupAnagrams = new HashMap<>();

        if (words == null || words.length == 0) {
            return Collections.EMPTY_LIST;
        }

        for (String word : words) {
            char[] charArray = word.toCharArray();
            Arrays.sort(charArray);

            if (!groupAnagrams.containsKey(Arrays.toString(charArray))) {
                groupAnagrams.put(Arrays.toString(charArray), new ArrayList<>());
            }

            groupAnagrams.get(Arrays.toString(charArray)).add(word);
        }

        return new ArrayList<List<String>>(groupAnagrams.values());
    }
}
