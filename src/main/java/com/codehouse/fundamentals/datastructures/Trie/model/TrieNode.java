package com.codehouse.fundamentals.datastructures.Trie.model;

import java.util.HashMap;
import java.util.Map;

public class TrieNode {

    private Map<Character, TrieNode> characterMap;
    private boolean isEndOfWord;

    public TrieNode() {
        this.characterMap = new HashMap<>();
        this.isEndOfWord = false;
    }

    public Map<Character, TrieNode> getCharacterMap() {
        return characterMap;
    }

    public boolean isEndOfWord() {
        return isEndOfWord;
    }

    public void setEndOfWord(boolean endOfWord) {
        isEndOfWord = endOfWord;
    }
}
