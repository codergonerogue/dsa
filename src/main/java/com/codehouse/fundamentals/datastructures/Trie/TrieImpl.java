package com.codehouse.fundamentals.datastructures.Trie;

import com.codehouse.fundamentals.datastructures.Trie.model.TrieNode;

/**
 * Trie Operations Implementation.
 */
public class TrieImpl implements Trie {
    private TrieNode root;

    public TrieImpl() {
        this.root = new TrieNode();
    }

    /**
     * Create a Trie of all the words in teh string.
     * @param input : String of words that need to be added to Trie.
     *              Each word should be separated by a comma.
     */
    public TrieImpl(String input) {
        if(input == null || input.length() == 0) {
            throw new IllegalArgumentException("Please pass a valid string/input");
        }

        new TrieImpl();

        if(input.split(",").length == 1) {
            insert(input);
        } else {
            String[] words = input.split(",");
            for(String word : words){
                insert(word.trim());
            }
        }

    }



    public TrieNode getRoot() {
        return root;
    }

    public void setRoot(TrieNode root) {
        this.root = root;
    }

    /**
     * {@inheritDoc}
     */
    public TrieNode insert(String word) {
        TrieNode current = this.root;
        for(char c : word.toCharArray()) {
            if (current.getCharacterMap().get(c) == null) {
                TrieNode node = new TrieNode();
                current.getCharacterMap().put(c, node);
            } else {
                current = current.getCharacterMap().get(c);
            }
        }
        current.setEndOfWord(true);

        return null;
    }

    /**
     * {@inheritDoc}
     */
    public boolean search(String word) {
        TrieNode current = this.root;

        for(char c : word.toCharArray()){

            if(current.getCharacterMap().get(c) == null){
                return false;
            }
            current = current.getCharacterMap().get(c);
        }
        return current.isEndOfWord();
    }

    /**
     * {@inheritDoc}
     */
    public boolean delete(String word) {
        return delete(this.root, word, 0);
    }

    private boolean delete(TrieNode current, String word, int index) {
        if(index == word.length()){
            if(!current.isEndOfWord()) {
                return false;
            }
            current.setEndOfWord(false);
            return current.getCharacterMap().size() == 0;
        }
        char ch = word.charAt(index);
        TrieNode node = current.getCharacterMap().get(ch);
        if (node == null) {
            return false;
        }
        boolean shouldDeleteCurrentNode = delete(node, word, index + 1);

        //if true is returned then delete the mapping of character and trienode reference from map.
        if (shouldDeleteCurrentNode) {
            current.getCharacterMap().remove(ch);
            //return true if no mappings are left in the map.
            return current.getCharacterMap().size() == 0;
        }
        return false;

    }
}
