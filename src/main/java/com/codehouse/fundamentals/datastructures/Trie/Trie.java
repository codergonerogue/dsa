package com.codehouse.fundamentals.datastructures.Trie;

import com.codehouse.fundamentals.datastructures.Trie.model.TrieNode;

/**
 * API for Trie operations
 */
public interface Trie {
    /**
     * Build a trie of a word, with each node representing the characters in the String
     * @param word: Set of characters that make up nodes of the Trie.
     * @return root node of the Trie.
     */
    TrieNode insert(String word);

    /**
     * Search for the entire word.
     * @param word The word you want to look up.
     * @return true or false depending on whether the complete word exists or not respectively.
     */
    boolean search(String word);

    /**
     * Delete an entire word and also the entire character map if there are no other words which use the {@param word}
     * as the prefix.
     * @param word The word you want to delete.
     * @return true or false depending on whether the word was deleted or not respectively.
     */
    boolean delete(String word);
}
