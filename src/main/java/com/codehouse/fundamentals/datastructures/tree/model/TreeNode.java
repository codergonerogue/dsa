package com.codehouse.fundamentals.datastructures.tree.model;

import com.codehouse.fundamentals.datastructures.tree.binarysearchtree.LowestCommonAncestor;

public class TreeNode<T> {
    private TreeNode<T> leftNode;

    public TreeNode<T> getLeftNode() {
        return leftNode;
    }

    public void setLeftNode(TreeNode<T> leftNode) {
        this.leftNode = leftNode;
    }

    public TreeNode<T> getRightNode() {
        return rightNode;
    }

    public void setRightNode(TreeNode<T> rightNode) {
        this.rightNode = rightNode;
    }

    public T getData() {
        return data;
    }

    public void setData(T data) {
        this.data = data;
    }

    private TreeNode<T> rightNode;
    private T data;

    public TreeNode(T data) {
        this.data = data;
        this.leftNode = null;
        this.rightNode = null;
    }

    public static TreeNode<Integer> buildBinaryTree() {
        TreeNode<Integer> node3 = new TreeNode<>(3);
        TreeNode<Integer> node6 = new TreeNode<>(6);
        TreeNode<Integer> node8 = new TreeNode<>(8);
        TreeNode<Integer> node2 = new TreeNode<>(2);
        TreeNode<Integer> node11 = new TreeNode<>(11);

        TreeNode<Integer> node9 = new TreeNode<>(9);
        ;
        TreeNode<Integer> node5 = new TreeNode<>(5);
        TreeNode<Integer> node13 = new TreeNode<>(13);
        TreeNode<Integer> node7 = new TreeNode<>(7);

        node3.setLeftNode(node6);
        node3.setRightNode(node8);

        node6.setLeftNode(node2);
        node6.setRightNode(node11);

        node11.setLeftNode(node9);
        node11.setRightNode(node5);

        node8.setRightNode(node13);
        node13.setLeftNode(node7);

        return node3;
    }

    public static TreeNode<Integer> buildBinarySearchTree() {
        TreeNode<Integer> node10 = new TreeNode<>(10);
        TreeNode<Integer> nodeminus10 = new TreeNode<>(-10);
        TreeNode<Integer> node8 = new TreeNode<>(8);
        TreeNode<Integer> node6 = new TreeNode<>(6);
        TreeNode<Integer> node9 = new TreeNode<>(9);

        TreeNode<Integer> node30 = new TreeNode<>(30);
        ;
        TreeNode<Integer> node25 = new TreeNode<>(25);
        TreeNode<Integer> node60 = new TreeNode<>(60);
        TreeNode<Integer> node28 = new TreeNode<>(28);
        TreeNode<Integer> node78 = new TreeNode<>(78);

        node10.setLeftNode(nodeminus10);
        node10.setRightNode(node30);

        nodeminus10.setRightNode(node8);

        node30.setLeftNode(node25);
        node30.setRightNode(node60);

        node8.setLeftNode(node6);
        node8.setRightNode(node9);

        node25.setRightNode(node28);
        node60.setRightNode(node78);

        return node10;
    }
}
