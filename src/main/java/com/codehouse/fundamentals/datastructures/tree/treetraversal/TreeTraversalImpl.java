package com.codehouse.fundamentals.datastructures.tree.treetraversal;

import com.codehouse.fundamentals.datastructures.tree.model.TreeNode;

import java.util.LinkedList;
import java.util.Queue;
import java.util.Stack;

public class TreeTraversalImpl<T> implements TreeTraversal<T> {
    @Override
    public void inorderRecursiveTraversal(TreeNode<T> root) {
        if (root == null) {
            return;
        }
        inorderRecursiveTraversal(root.getLeftNode());
        visitNode(root);
        inorderRecursiveTraversal(root.getRightNode());
    }

    @Override
    public void preOrderRecursiveTraversal(TreeNode<T> root) {
        if (root == null) {
            return;
        }
        visitNode(root);
        preOrderRecursiveTraversal(root.getLeftNode());
        preOrderRecursiveTraversal(root.getRightNode());
    }

    @Override
    public void postOrderRecursiveTraversal(TreeNode<T> root) {
        if (root == null) {
            return;
        }
        postOrderRecursiveTraversal(root.getLeftNode());
        postOrderRecursiveTraversal(root.getRightNode());
        visitNode(root);
    }

    @Override
    public void levelOrderTraversal(TreeNode<T> root) {
        Queue<TreeNode<T>> nodeQueue = new LinkedList<>();

        if (root == null) {
            return;
        }

        nodeQueue.add(root);

        while (!nodeQueue.isEmpty()) {
            root = nodeQueue.poll();
            visitNode(root);

            if (root.getLeftNode() != null) {
                nodeQueue.add(root.getLeftNode());
            }

            if (root.getRightNode() != null) {
                nodeQueue.add(root.getRightNode());
            }
        }

    }

    @Override
    public void inOrderTraversalIterativeStack(TreeNode<T> root) {
        if (root == null) {
            return;
        }

        Stack<TreeNode<T>> stack = new Stack<>();
        TreeNode<T> currentNode = root;

        while (true) {

            if(currentNode != null) {
                stack.push(currentNode);
                currentNode = currentNode.getLeftNode();
            } else {

                if(stack.isEmpty()) {
                    break;
                }

                currentNode = stack.pop();
                visitNode(currentNode);
                currentNode = currentNode.getRightNode();
            }
        }
    }

    @Override
    public void postOrderTraversalIterative(TreeNode<T> root) {

    }


    private void visitNode(TreeNode<T> root) {
        System.out.println(root.getData());
    }

}
