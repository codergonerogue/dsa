package com.codehouse.fundamentals.datastructures.tree.binarytree.problems;

import com.codehouse.fundamentals.datastructures.tree.model.TreeNode;

import java.util.LinkedList;
import java.util.Queue;

public class InvertbinaryTree {
    public TreeNode<Integer> invertTree(TreeNode<Integer> root) {
        Queue<TreeNode<Integer>> queue = new LinkedList<>();
        if(root == null) {
            return null;
        }

        queue.add(root);

        while(!queue.isEmpty()) {
            TreeNode temp = queue.peek();
            queue.add(temp.getRightNode());
            queue.add(temp.getLeftNode());
            invertTreeHelper(temp, queue);
        }
        return root;
    }

    private static void invertTreeHelper(TreeNode<Integer> treeNode, Queue<TreeNode<Integer>> nodeQueue) {
        TreeNode<Integer> rNode = nodeQueue.poll();
        treeNode.setLeftNode(rNode);

        TreeNode<Integer> lNode = nodeQueue.poll();
        treeNode.setRightNode(lNode);
    }
}
