package com.codehouse.fundamentals.datastructures.tree.binarytree.problems;

import com.codehouse.fundamentals.datastructures.tree.model.TreeNode;

public class MaximumDepthOfBinaryTree {
    public static int maxDepthOfBinaryTree(TreeNode<Integer> root) {
        if(root == null) {
            // base case
            // Simplest unit of the problem
            // base case is when the tree has no node(s).
            return 0;
        } else {
            // recursive case
            // Handle part of the problem myself and hand over the complex part for recursion
            int leftHeight = maxDepthOfBinaryTree(root.getLeftNode()) + 1;
            int rightHeight = maxDepthOfBinaryTree(root.getRightNode()) + 1;
            return Integer.max(leftHeight, rightHeight);
        }
    }
}

