package com.codehouse.fundamentals.datastructures.tree.binarysearchtree;

import com.codehouse.fundamentals.datastructures.tree.model.TreeNode;

public class SearchInBST {
    public TreeNode<Integer> searchBST(TreeNode<Integer> root, int val) {
        if(root == null || root.getData() == val) {
            return root;
        }

        if(root.getData() > val) {
            return searchBST(root.getLeftNode(), val);
        } else {
            return searchBST(root.getRightNode(), val);
        }
    }
}
