package com.codehouse.fundamentals.datastructures.tree.binarytree.problems;

import com.codehouse.fundamentals.datastructures.tree.model.TreeNode;

import java.util.LinkedList;
import java.util.Queue;

public class TreeToString {

    public static void main(String[] args) {
        TreeNode<Integer> node1 = new TreeNode<>(1);
        TreeNode<Integer> node2 = new TreeNode<>(2);
        TreeNode<Integer> node3 = new TreeNode<>(3);
        TreeNode<Integer> node4 = new TreeNode<>(4);

        node1.setLeftNode(node2);
        node1.setRightNode(node3);
        node2.setRightNode(node4);

        System.out.println(tree2str(node1));
    }

    public static String tree2str(final TreeNode t) {
        StringBuilder resultBuilder = new StringBuilder();
        Queue<TreeNode> queue = new LinkedList();

        if (t == null) {
            return resultBuilder.toString();
        }

        queue.add(t);

        while (!queue.isEmpty()) {
            TreeNode<Integer> tNode = queue.poll();
            if (tNode == null) {
                resultBuilder.append("()");
                queue.remove();
            } else {
                resultBuilder.append("(");
                resultBuilder.append(tNode.toString());
                resultBuilder.append(")");
            }
            queue.add(tNode.getLeftNode());
            queue.add(tNode.getRightNode());
        }
        return resultBuilder.toString();
    }
}
