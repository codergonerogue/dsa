package com.codehouse.fundamentals.datastructures.tree.binarysearchtree;

import com.codehouse.fundamentals.datastructures.tree.model.TreeNode;

public class LowestCommonAncestor {

    public TreeNode<Integer> lowestCommonAncestor(final TreeNode<Integer> root, final int data1, final int data2) {
        if (root.getData() > data1 && root.getData() > data2) {
            return lowestCommonAncestor(root.getLeftNode(), data1, data2);
        } else if (root.getData() < data1 && root.getData() < data2) {
            return lowestCommonAncestor(root.getRightNode(), data1, data2);
        } else {
            return root;
        }
    }

    public static void main(String[] args) {
        TreeNode<Integer> root = TreeNode.buildBinarySearchTree();

        LowestCommonAncestor lca = new LowestCommonAncestor();
        TreeNode result = lca.lowestCommonAncestor(root, 6, 9);

        System.out.println(result.getData());

    }

}
