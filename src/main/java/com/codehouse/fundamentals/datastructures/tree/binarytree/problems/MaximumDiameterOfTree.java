package com.codehouse.fundamentals.datastructures.tree.binarytree.problems;

import com.codehouse.fundamentals.datastructures.tree.model.TreeNode;

public class MaximumDiameterOfTree {

    static int maxDiameter;

    public static void main(String[] args) {
        TreeNode<Integer> root = new TreeNode<>(1);
        TreeNode<Integer> left1 = new TreeNode<>(2);
        TreeNode<Integer> right1 = new TreeNode<>(3);
        TreeNode<Integer> left21 = new TreeNode<>(4);
        TreeNode<Integer> left22 = new TreeNode<>(5);
        TreeNode<Integer> right21 = new TreeNode<>(5);
        TreeNode<Integer> right22 = new TreeNode<>(5);

        root.setLeftNode(left1);
        root.setRightNode(right1);

        left1.setLeftNode(left21);
        left1.setRightNode(right21);

        right1.setLeftNode(left22);
        right1.setRightNode(right22);

        MaximumDiameterOfTree maxDia = new MaximumDiameterOfTree();
        maxDia.maxDiameterOfBinaryTree(root);
        System.out.println(maxDiameter);
    }

    public void maxDiameterOfBinaryTree(TreeNode node) {
        maxDiameter = 0;
        maxDepth(node);
    }

    public int maxDepth(final TreeNode node) {
        if (node == null) {
            return 0;
        }

        int leftSubTreeDia = maxDepth(node.getLeftNode());
        int rightSubTreeDia = maxDepth(node.getRightNode());

        maxDiameter = Math.max(maxDiameter, leftSubTreeDia+rightSubTreeDia);

        return Math.max(leftSubTreeDia, rightSubTreeDia) + 1;

    }
}
