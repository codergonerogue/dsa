package com.codehouse.fundamentals.datastructures.tree.treetraversal;

import com.codehouse.fundamentals.datastructures.tree.model.TreeNode;

public interface TreeTraversal<T> {

    void inorderRecursiveTraversal(TreeNode<T> root);

    void preOrderRecursiveTraversal(TreeNode<T> root);

    void postOrderRecursiveTraversal(TreeNode<T> root);

    void levelOrderTraversal(TreeNode<T> root);

    void inOrderTraversalIterativeStack(TreeNode<T> root);

    void postOrderTraversalIterative(TreeNode<T> root);
}
