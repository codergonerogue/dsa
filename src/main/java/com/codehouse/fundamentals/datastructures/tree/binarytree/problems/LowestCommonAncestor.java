package com.codehouse.fundamentals.datastructures.tree.binarytree.problems;

import com.codehouse.fundamentals.datastructures.tree.model.TreeNode;

public class LowestCommonAncestor {

    public static void main(String[] args) {
        TreeNode<Integer> root = TreeNode.buildBinaryTree();
        LowestCommonAncestor lowestCommonAncestor = new LowestCommonAncestor();
        TreeNode result = lowestCommonAncestor.lowestCommonAncestor(root, 6, 9);
        System.out.println(result.getData());
    }

    public TreeNode lowestCommonAncestor(final TreeNode<Integer> root, final int data1, final int data2) {

        if (root == null) {
            return null;
        }

        if (root.getData() == data1 || root.getData() == data2) {
            return root;
        }

        TreeNode leftRecurrenceResult = lowestCommonAncestor(root.getLeftNode(), data1, data2);
        TreeNode rightRecurrenceResult = lowestCommonAncestor(root.getRightNode(), data1, data2);

        if (leftRecurrenceResult != null && rightRecurrenceResult != null) {
            return root;
        }

        if (leftRecurrenceResult == null && rightRecurrenceResult == null) {
            return null;
        }

        return leftRecurrenceResult != null ? leftRecurrenceResult : rightRecurrenceResult;
    }
}