package com.codehouse.fundamentals.datastructures.tree.binarysearchtree;

import com.codehouse.fundamentals.datastructures.tree.model.TreeNode;
import com.codehouse.fundamentals.datastructures.tree.treetraversal.TreeTraversal;
import com.codehouse.fundamentals.datastructures.tree.treetraversal.TreeTraversalImpl;

import java.util.Arrays;

public class SortedArrayToBST {
    public static void main(String[] args) {
        int[] elements = {1, 2, 3, 4, 5, 6, 7, 8};
        TreeNode<Integer> root = SortedArrayToBST.sortedArrayToBST(elements);

        TreeTraversal<Integer> traverser = new TreeTraversalImpl<>();
        traverser.inorderRecursiveTraversal(root);
    }

    public static TreeNode<Integer> sortedArrayToBST(final int[] elements) {
        if(elements == null || elements.length == 0) {
            return null;
        }

        int mid = elements.length / 2;
        TreeNode<Integer> root = new TreeNode<>(elements[mid]);

        TreeNode<Integer> leftNode = sortedArrayToBST(Arrays.copyOfRange(elements, 0, mid));
        root.setLeftNode(leftNode);

        TreeNode<Integer> rightNode = sortedArrayToBST(Arrays.copyOfRange(elements, mid+1, elements.length));
        root.setRightNode(rightNode);

        return root;
    }
}
