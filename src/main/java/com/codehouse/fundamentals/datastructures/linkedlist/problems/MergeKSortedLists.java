package com.codehouse.fundamentals.datastructures.linkedlist.problems;

import com.codehouse.fundamentals.datastructures.linkedlist.model.ListNode;

public class MergeKSortedLists {
    public ListNode mergeKLists(ListNode[] lists) {
        if (lists == null || lists.length == 0) {
            return null;
        }

        int i = 0;
        int j = lists.length - 1;
        MergeTwoSortedList mergeTwoSortedList = new MergeTwoSortedList();
        while (i != j) {
            lists[i] = mergeTwoSortedList.mergeTwoListsRecursive(lists[i], lists[j]);
            j--;
        }
        return lists[0];
    }

    static void printList(ListNode head) {
        while (head != null) {
            System.out.print(head.val + " ");
            head = head.next;
        }
        System.out.println("");
    }

    public static void main(String[] args) {
        ListNode head1, head2, head3;
        // creating first list
        head1 = new ListNode(1);
        head1.next = new ListNode(2);
        head1.next.next = new ListNode(7);

        // creating seconnd list
        head2 = new ListNode(1);
        head2.next = new ListNode(3);
        head2.next.next = new ListNode(4);

        head3 = new ListNode(5);
        head3.next = new ListNode(8);

        ListNode[] lists = new ListNode[3];
        lists[0] = head1;
        lists[1] = head2;
        lists[2] = head3;

        for (int i = 0; i < lists.length; i++) {
            printList(lists[i]);
        }

        MergeKSortedLists mergeKSortedLists = new MergeKSortedLists();
        ListNode head = mergeKSortedLists.mergeKLists(lists);

        printList(head);
    }
}
