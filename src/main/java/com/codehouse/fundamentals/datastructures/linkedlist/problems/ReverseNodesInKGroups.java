package com.codehouse.fundamentals.datastructures.linkedlist.problems;

import com.codehouse.fundamentals.datastructures.linkedlist.model.Node;

public class ReverseNodesInKGroups {
    public Node reverseKGroup(Node head, int k) {
        //1. test weather we have more then k node left, if less then k node left we just return head
        Node node = head;
        int count = 0;
        while (count < k) {
            if(node == null)return head;
            node = node.getNext();
            count++;
        }
        // 2.reverse k node at current level
        Node pre = reverseKGroup(node, k); //pre node point to the the answer of sub-problem
        while (count > 0) {
            Node next = head.getNext();
            head.setNext(pre);
            pre = head;
            head = next;
            count = count - 1;
        }
        return pre;
    }
}
