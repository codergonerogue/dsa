package com.codehouse.fundamentals.datastructures.linkedlist.problems;

import com.codehouse.fundamentals.datastructures.linkedlist.model.ListNode;

public class SumOfTwoNumbers {
    static ListNode head1, head2;

    public ListNode getResult(ListNode l1, ListNode l2) {
        int carry = 0;
        ListNode prevSum = new ListNode(0);
        ListNode head = prevSum;

        while(l1 != null || l2 != null || carry != 0) {
            ListNode currentNode = new ListNode(0);

            int sum = (l1 == null ? 0 : l1.val)
                    + (l2 == null ? 0 : l2.val)
                    + carry;
            currentNode.val = sum % 10;
            carry = sum / 10;

            prevSum.next = currentNode;
            prevSum = currentNode;

            l1 = (l1 == null) ? l1 : l1.next;
            l2 = (l2 == null) ? l2 : l2.next;

        }

        return head.next;
    }

    void printList(ListNode head) {
        while (head != null) {
            System.out.print(head.val + " ");
            head = head.next;
        }
        System.out.println("");
    }

    public static void main(String[] args) {
        SumOfTwoNumbers list = new SumOfTwoNumbers();

        // creating first list
        list.head1 = new ListNode(1);
        list.head1.next = new ListNode(1);
        list.head1.next.next = new ListNode(9);
        System.out.print("First List is ");
        list.printList(head1);

        // creating seconnd list
        list.head2 = new ListNode(9);
        list.head2.next = new ListNode(2);
        list.head2.next.next = new ListNode(2);
        System.out.print("Second List is ");
        list.printList(head2);

        ListNode rs = list.getResult(head1, head2);
        System.out.print("Result List is ");
        list.printList(rs);

    }
}
