package com.codehouse.fundamentals.datastructures.linkedlist.problems;

import com.codehouse.fundamentals.datastructures.linkedlist.model.ListNode;

public class MergeTwoSortedList {

    public static ListNode mergeTwoListsRecursive(ListNode l1, ListNode l2) {
        // base cases
        if (l1 == null) {
            // do one unit of work.
            return l2;
        }
        if (l2 == null) {
            return l1;
        }

        ListNode result = null;
        if(l1.val <= l2.val) {
            result = l1;
            result.next = mergeTwoListsRecursive(l1.next, l2);
        } else {
            result = l2;
            result.next = mergeTwoListsRecursive(l1, l2.next);
        }

        return result;
    }

    static void printList(ListNode head) {
        while (head != null) {
            System.out.print(head.val + " ");
            head = head.next;
        }
        System.out.println("");
    }

    public static void main(String[] args) {
        MergeTwoSortedList list = new MergeTwoSortedList();
        ListNode head1, head2;
        // creating first list
        head1 = new ListNode(1);
        head1.next = new ListNode(2);
        head1.next.next = new ListNode(4);

        // creating seconnd list
        head2 = new ListNode(1);
        head2.next = new ListNode(3);
        head2.next.next = new ListNode(4);

        ListNode head = mergeTwoListsRecursive(head1, head2);
        printList(head);
    }
}
