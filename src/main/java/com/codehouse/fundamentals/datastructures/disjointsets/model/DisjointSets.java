package com.codehouse.fundamentals.datastructures.disjointsets.model;

import com.codehouse.fundamentals.datastructures.disjointsets.IDisjointSets;

import java.util.HashMap;
import java.util.Map;

public class DisjointSets implements IDisjointSets {
    Map<Integer, SetNode> dataNode = new HashMap<>();

    /**
     * {@inheritDoc}
     */
    public void makeSet(int data) {
        SetNode node = new SetNode(data);
        node.parent = node;
        node.rank = 0;
        dataNode.put(data, node);
    }

    /**
     * {@inheritDoc}
     */
    public void makeSet(int[] data) {
        for (int element : data) {
            makeSet(element);
        }
    }

    /**
     * {@inheritDoc}
     *
     * @param data1
     * @param data2
     */
    public void union(int data1, int data2) {

        SetNode parent1 = findSet(dataNode.get(data1));
        SetNode parent2 = findSet(dataNode.get(data2));

        if (parent1.data == parent2.data) {
            return;
        }


        if (parent1.rank >= parent2.rank) {

            if (parent1.rank == parent2.rank) {
                parent1.rank = parent1.rank + 1;
            }

            parent2.parent = parent1;

        } else {
            parent1.parent = parent2;
        }

    }

    /**
     * {@inheritDoc}
     *
     * @param node
     */
    public SetNode findSet(SetNode node) {
        if (node.parent == node) {
            return node;
        }
        node.parent = findSet(node.parent);

        return node.parent;
    }
}
