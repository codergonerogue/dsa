package com.codehouse.fundamentals.datastructures.disjointsets.model;

public class SetNode {
    //Stores the apprximate depth of the tree.
    int rank;

    //Stores the data attribute.
    int data;

    //Pointer from child to parent.
    SetNode parent;

    public SetNode(int data) {
        this.data = data;
    }
}
