package com.codehouse.fundamentals.datastructures.disjointsets;

import com.codehouse.fundamentals.datastructures.disjointsets.model.SetNode;

public interface IDisjointSets {

    /**
     * MakeSet creates a set with a single element in it.
     * Basically initializes the SetNode data-structure for a given data element.
     * Sets the parent attribute to the node itself
     * Initializes the data attribute with the incoming data.
     * Initializes rank to 0
     *
     * @param data
     */
    void makeSet(int data);

    /**
     * Operate on all of the elements of the input dataset using {@link IDisjointSets#makeSet(int)}
     *
     * @param data
     */
    void makeSet(int[] data);

    /**
     * Creates an union of the two nodes.
     * To create a union we folow the below algorithm.
     *
     * @param data1
     * @param data2
     */
    void union(int data1, int data2);

    /**
     * Recursively find the root of a given node.
     *
     * @param node
     * @return
     */
    SetNode findSet(SetNode node);


}
