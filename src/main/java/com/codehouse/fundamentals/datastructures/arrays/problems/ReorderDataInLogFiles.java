package com.codehouse.fundamentals.datastructures.arrays.problems;

public class ReorderDataInLogFiles {
    public String[] reorderLogFiles(String[] logs) {
        int i = 0;
        int j = logs.length - 1;
        while (i < j) {
            if (isDigitLog(logs[i])) {
                if (isDigitLog(logs[j])) {

                }
            } else {

            }
        }
        return logs;
    }

    private boolean isDigitLog(String log) {
        String logType = log.substring(log.indexOf(" ")+1).substring(0, log.indexOf(" ") - 1);
        try {
            Long.parseLong(logType);
            return true;
        } catch (NumberFormatException e) {
            return false;
        }
    }

    public static void main(String[] args) {
        String[] logs = {"dig1 8 1 5 1","let1 art can","dig2 3 6","let2 own kit dig","let3 art zero"};
    }
}
