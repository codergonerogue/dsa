package com.codehouse.fundamentals.datastructures.arrays.problems;

public class FindMinumumInRotatedSortedArray {
    public int findMin(int[] nums) {
        if (nums == null || nums.length == 0) {
            return 0;
        }
        if (nums.length == 1 || (nums[0] < nums[nums.length - 1])) {
            return nums[0];
        }

        return findMinimum(nums);
    }

    private int findMinimum(int[] nums) {
        int low = 0;
        int high = nums.length - 1;

        while (high >= low) {
            int mid = low + (high - low) / 2;
            if (nums[mid] > nums[mid+1]) {
                return nums[mid+1];
            }

            if (nums[mid] < nums[mid-1]) {
                return nums[mid];
            }

            if (nums[mid] > nums[0]) {
                low = mid + 1;
            } else {
                high = mid - 1;
            }
        }
        return -1;
    }
}
