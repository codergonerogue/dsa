package com.codehouse.fundamentals.datastructures.arrays.problems;

import java.util.Comparator;
import java.util.PriorityQueue;

public class KthLargestElementInArray215 {
    public static int findKthLargest(int[] nums, int k) {
        int sizeOfHeap = nums.length;
        PriorityQueue<Integer> maxHeap = new PriorityQueue<Integer>(sizeOfHeap, new Comparator<Integer>() {
            @Override
            public int compare(Integer o1, Integer o2) {
                return Integer.compare(o1, o2);
            }
        });

        for(int num : nums) {
            maxHeap.add(num);
//            if (maxHeap.size() > k) {
//                maxHeap.poll();
//            }
        }

        return maxHeap.poll();
    }

    public static void main(String[] args) {
        int[] nums = {7, 10, 4, 3, 20, 15};
        System.out.println(findKthLargest(nums, 4));
    }
}
