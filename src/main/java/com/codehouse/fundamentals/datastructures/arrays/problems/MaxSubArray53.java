package com.codehouse.fundamentals.datastructures.arrays.problems;


public class MaxSubArray53 {
    /**
     * #53
     * Given an integer array nums, find the contiguous subarray (containing at least one number) which has the largest sum and return its sum.
     *
     * Example:
     * Input: [-2,1,-3,4,-1,2,1,-5,4],
     * Output: 6
     * Explanation: [4,-1,2,1] has the largest sum = 6.
     * Follow up:
     *
     * If you have figured out the O(n) solution, try coding another solution using the divide and conquer approach, which is more subtle.
     * @param nums
     * @return
     */
    public static int maxSubArray(int[] nums) {
        if (nums.length == 0) {
            return 0;
        } else if (nums.length == 1) {
            return nums[0];
        }

        int maxEndingHere = nums[0];
        int maxSoFar = maxEndingHere;

        for(int i = 1; i < nums.length; ++i) {

            if (maxEndingHere < 0) {
                // If the CURRENT-VALUE of max ending here (Sum of A[0....i-1])
                // is less than 0 (negative) then adding anything to it will yield a sum
                // that is less than the value at i.
                maxEndingHere = nums[i];
            } else {
                maxEndingHere = maxEndingHere + nums[i];
            }

            if (maxSoFar < maxEndingHere) {
                maxSoFar = maxEndingHere;
            }
        }
        return maxSoFar;
    }

    public static void main(String[] args) {
        //int[] input = {-2, -3};
        int[] input = {7, 1, 5, 3, 6, 4};

        System.out.print(maxSubArray(input));
    }
}
