package com.codehouse.fundamentals.datastructures.arrays.problems;

public class MergeTwoSortedArrays {
    public static void merge(int[] nums1, int m, int[] nums2, int n) {
        int L = nums1.length;
        while(m != 0 && n != 0) {
            if (nums1[m-1] > nums2[n-1]) {
                nums1[L-1] = nums1[m-1];
                m = m-1;
            } else {
                nums1[L-1] = nums2[n-1];
                n = n-1;
            }
            L = L - 1;
        }

        if (m == 0) {
            int i = 0;
            while (i < n) {
                nums1[i] = nums2[i];
                i++;
            }
        }
    }

    public static void main(String[] args) {
        int[] nums1 = {2, 0};
        int[] nums2 = {1};
        int m = 1;
        int n = 1;
        merge(nums1, m, nums2, n);
        for(int i = 0; i < nums1.length; i++){
            System.out.print(nums1[i]+"\t");
        }
    }
}
