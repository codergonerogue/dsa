package com.codehouse.fundamentals.datastructures.arrays.problems;

import java.util.HashSet;
import java.util.Set;

public class IntersectionOfTwoSortedArray {
    public int[] intersection(int[] nums1, int[] nums2) {
        if (nums1 == null || nums2 == null) {
            return new int[0];
        }

        int l1 = nums1.length;
        int l2 = nums2.length;
        Set<Integer> result = new HashSet<>();
        if (l1 < l2) {
            for (int i : nums2) {
                if (binarySearch(nums1, i)) {
                    result.add(i);
                }
            }
        } else {
            for (int i : nums1) {
                if (binarySearch(nums2, i)) {
                    result.add(i);
                }
            }
        }

        int[] resultArray = new int[result.size()];
        int k = 0;
        for (Integer i : result) {
            resultArray[k] = i;
            k++;
        }
        return resultArray;
    }

    boolean binarySearch(int[] array, int target) {
        int low = 0;
        int high = array.length - 1;
        int mid;

        while (low <= high) {
            mid = low + (high - low) / 2;
            if (array[mid] == target) {
                return true;
            } else if (target < array[mid]) {
                high = mid - 1;
            } else {
                low = mid + 1;
            }
        }
        return false;
    }

    public static void main(String[] args) {
        IntersectionOfTwoSortedArray obj = new IntersectionOfTwoSortedArray();
        int[] nums1 = {1, 1, 2, 2};
        int[] nums2 = {1, 2, 2};
        for(int i : obj.intersection(nums1, nums2)) {
            System.out.print(i+"\t");
        }
    }
}
