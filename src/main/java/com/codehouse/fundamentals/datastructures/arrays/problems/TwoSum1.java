package com.codehouse.fundamentals.datastructures.arrays.problems;

import java.util.HashMap;

public class TwoSum1 {
    // Sorted input
    public static int[] twoSumSorted(int[] numbers, int target) {
        int lowPointer = 0;
        int highPointer = numbers.length - 1;
        for(int i = 0; i < numbers.length; i++) {
            if(numbers[lowPointer] + numbers[highPointer] == target) {
                break;
            } else if(numbers[lowPointer] + numbers[highPointer] < target) {
                lowPointer++;
            } else {
                highPointer--;
            }
        }
        int[] result =  { lowPointer+1, highPointer+1 };
        return result;
    }

    // Unsorted input
    public static int[] twoSumUnSorted(int[] nums, int target) {
        int[] result = new int[2];
        HashMap<Integer, Integer> sums = new HashMap<>();

        for(int i = 0; i < nums.length; i++) {
            int diff = target - nums[i];
            if(sums.get(diff) != null) {
                result[0] = sums.get(diff);
                result[1] = i;
                break;
            } else {
                sums.put(nums[i], i);
            }
        }
        return result;
    }

    public static void main(String[] args) {
        int[] inputSorted = { 1, 2, 3, 4, 4, 9, 56, 90 };
        int target = 8;
        int[] result = TwoSum1.twoSumSorted(inputSorted, target);
        displayResult(result);

        int[] inputUnSorted = { 2, 11, 7, 15 };
        target = 9;
        result = TwoSum1.twoSumUnSorted(inputUnSorted, target);
        displayResult(result);
    }

    private static void displayResult(int[] result) {
        for(int num : result) {
            System.out.print(num);
        }
    }
}
