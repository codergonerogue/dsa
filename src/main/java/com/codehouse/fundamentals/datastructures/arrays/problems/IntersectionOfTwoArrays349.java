package com.codehouse.fundamentals.datastructures.arrays.problems;

import java.util.HashSet;
import java.util.Set;

public class IntersectionOfTwoArrays349 {

    public static int[] intersection(int[] nums1, int[] nums2) {
        if (nums1 == null || nums2 == null) {
            return new int[0];
        }

        int l1 = nums1.length;
        int l2 = nums2.length;

        Set<Integer> set1 = new HashSet<Integer>();
        Set<Integer> set2 = new HashSet<Integer>();

        for (int i : nums1) {
            set1.add(i);
        }

        for (int i : nums2) {
            set2.add(i);
        }

        int[] result;
        if (l1 < l2) {
            set2.retainAll(set1);
            result = new int[set2.size()];
            int k = 0;
            for (Integer i : set2) {
                result[k] = i;
                k++;
            }
        } else {
            set1.retainAll(set2);
            result = new int[set1.size()];
            int k = 0;
            for (Integer i : set1) {
                result[k] = i;
                k++;
            }
        }

        return result;
    }

    public static void main(String[] args) {
        int[] nums1 = {1, 2, 2, 1};
        int[] nums2 = {2, 2};
        intersection(nums1, nums2);
    }
}
