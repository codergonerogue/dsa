package com.codehouse.fundamentals.datastructures.arrays.problems;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class ThreeSum15 {
    /**
     * Three Sum
     *
     * @param input
     * @return
     */
    public static List<List<Integer>> threeSum(int[] input) {
        List<List<Integer>> result = new ArrayList<>();
        int n = input.length;
        boolean found = false;
        // x + y + z = 0
        // x + y = -z
        for (int i = 0; i < n - 1; i++) {
            Set<Integer> nums = new HashSet<Integer>();
            for (int j = i + 1; j < n; j++) {
                int z = -(input[i] + input[j]);
                if (nums.contains(z)) {
                    List<Integer> list = Arrays.asList(input[i], input[j], z);
                    result.add(list);
                } else {
                    nums.add(input[j]);
                }
            }
        }
        return result;
    }

    public List<List<Integer>> threeSumUniqueResult(int[] nums) {
        List<List<Integer>> res = new ArrayList<>();
        Arrays.sort(nums);
        for (int i = 0; i + 2 < nums.length; i++) {
            if (i > 0 && nums[i] == nums[i - 1]) {              // skip same result
                continue;
            }
            int j = i + 1, k = nums.length - 1;
            int target = -nums[i];
            while (j < k) {
                if (nums[j] + nums[k] == target) {
                    res.add(Arrays.asList(nums[i], nums[j], nums[k]));
                    j++;
                    k--;
                    while (j < k && nums[j] == nums[j - 1]) j++;  // skip same result
                    while (j < k && nums[k] == nums[k + 1]) k--;  // skip same result
                } else if (nums[j] + nums[k] > target) {
                    k--;
                } else {
                    j++;
                }
            }
        }
        return res;
    }

    public static void main(String[] args) {

        //three sum
        int[] threeSumInput = {0, -1, 2, -3, 1, -2};
        List<List<Integer>> threeSumResult = threeSum(threeSumInput);
        for (List<Integer> list : threeSumResult) {
            for (Integer i : list) {
                System.out.print(i + "\t");
            }
            System.out.println();
        }

    }
}
