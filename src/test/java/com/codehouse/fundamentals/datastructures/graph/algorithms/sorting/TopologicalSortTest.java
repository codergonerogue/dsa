package com.codehouse.fundamentals.datastructures.graph.algorithms.sorting;

import com.codehouse.fundamentals.datastructures.graphs.algorithms.sorting.TopologicalSort;
import com.codehouse.fundamentals.datastructures.graphs.model.Graph;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

@RunWith(JUnit4.class)
public class TopologicalSortTest {
    @Test
    public void MinHeap_ArrayOfKeys_ConstructsHeapWithIndexAsWeight() {
        Graph graph = new Graph(7, false);
        graph.addEdge(0, 1);
        graph.addEdge(1, 3);
        graph.addEdge(0, 2);
        graph.addEdge(2, 4);
        graph.addEdge(3, 4);
        graph.addEdge(3, 5);
        graph.addEdge(4, 5);
        graph.addEdge(6, 2);

        TopologicalSort topSort = new TopologicalSort(graph);
        topSort.topologicalSort();
    }
}
