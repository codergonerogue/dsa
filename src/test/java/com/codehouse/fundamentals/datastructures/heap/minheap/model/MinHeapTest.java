package com.codehouse.fundamentals.datastructures.heap.minheap.model;

import org.junit.Test;
import org.junit.internal.runners.JUnit4ClassRunner;
import org.junit.runner.RunWith;

@RunWith(JUnit4ClassRunner.class)
public class MinHeapTest {

    @Test
    public void MinHeap_ArrayOfKeys_ConstructsHeapWithIndexAsWeight() {
        Character[] keys = {'A', 'B', 'C'};
        MinHeap<Character> heap = new MinHeap<>(keys);
        printHeap(heap);
    }

    @Test
    public void MinHeap_AddingWithRandomWeights_ConstructHeapWithCorrectOrderAndShape() {
        MinHeap<String> heap = new MinHeap<>();
        heap.addNode("E", 30);
        heap.addNode("A", 9);
        heap.addNode("B", 11);
        heap.addNode("D", 20);
        heap.addNode("C", 12);
        heap.addNode("G", 6);
        heap.addNode("F", 2);
        printHeap(heap);
    }

    @Test
    public void MinHeap_PollingTest() {
        MinHeap<String> heap = new MinHeap<>();
        heap.addNode("E", 30);
        heap.addNode("A", 9);
        heap.addNode("B", 11);
        heap.addNode("D", 20);
        heap.addNode("C", 12);
        heap.addNode("G", 6);
        heap.addNode("F", 2);

        printHeap(heap);

        heap.poll();

        printHeap(heap);

    }

    private <T> void printHeap(MinHeap<T> heap) {
        System.out.println("HEAP NODES");
        for (HeapNode<T> c : heap.getHeapNodes()) {
            System.out.println("(" + c.getKey() + "," + c.getWeight() + ")");
        }

        System.out.println("\nHEAP INDEX MAP");
        for (T key : heap.getNodePositions().keySet()) {
            System.out.println("(" + key + "->" + heap.getNodePositions().get(key) + ")");
        }
    }

}
